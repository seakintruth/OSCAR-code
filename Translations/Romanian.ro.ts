<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ro_RO" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;Despre</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="125"/>
        <source>Release Notes</source>
        <translation>Note despre actualizare</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Multumiri</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>Licenta generala publica</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation>Inchide</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="43"/>
        <source>Show data folder</source>
        <translation>Arata dosarul cu datele</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="47"/>
        <source>About OSCAR</source>
        <translation>Despre OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="91"/>
        <source>Sorry, could not locate About file.</source>
        <translation>Imi pare rau, nu am gasit fisierul Despre.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="104"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation>Imi pare rau, nu am gasit fisierul Multumiri.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="116"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation>Imi pare rau, nu am gasit fisierul Note despre actualizare.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="126"/>
        <source>OSCAR v%1</source>
        <translation>OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Important:</source>
        <translation>Important:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="130"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceding, because attempting to roll back later may break things.</source>
        <translation type="vanished">Aceasta fiind o versiune beta, este recomandat sa &lt;b&gt;faceti manual un backup la dosarul care contine datele &lt;/b&gt; inainte de a merge mai departe, deoarece daca doriti sa reveniti la versiune anterioara s-ar putea sa nu mai gasiti toate la locul lor.</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="142"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Pentru a vedea daca exista o licenta in limba dvs, vedeti %1.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="878"/>
        <source>Could not find the oximeter file:</source>
        <translation>Nu am gasit fisierul cu oximetria:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="884"/>
        <source>Could not open the oximeter file:</source>
        <translation>Nu am putut deschide fisierul cu oximetria:</translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Nu am putut transfera datele din pulsoximetru.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Asigurati-va mai intai ca ati selectat &apos;upload&apos; din meniul pulsoximetrului.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation>Nu am gasit fisierul cu oximetria:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation>Nu am putut deschide fisierul cu oximetria:</translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Mergi la ziua precedenta</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Arata sau ascunde calendarul</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Mergi la ziua urmatoare</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Mergi la cea mai recenta zi care are date inregistrate</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Evenimente</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translatorcomment>/Dimensiunea ferestrei de vizualizare</translatorcomment>
        <translation>Vezi dimensiunea</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1404"/>
        <source>Notes</source>
        <translation>Note</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>Jurnal</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation> eu </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1066"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1081"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1094"/>
        <source>Color</source>
        <translation>Culoare</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1116"/>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Small</source>
        <translation>Mic</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1131"/>
        <source>Medium</source>
        <translation>Mediu</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1136"/>
        <source>Big</source>
        <translation>Mare</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1195"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1208"/>
        <source>I&apos;m feeling ...</source>
        <translation>Ma simt ...</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1224"/>
        <source>Weight</source>
        <translation>Greutate</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1231"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1301"/>
        <source>Awesome</source>
        <translation>Super</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1339"/>
        <source>B.M.I.</source>
        <translation>I.M.C.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1355"/>
        <source>Bookmarks</source>
        <translation>Semne de carte</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1376"/>
        <source>Add Bookmark</source>
        <translation>Adauga Semn de carte (Bookmark)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1399"/>
        <source>Starts</source>
        <translation>Porniri</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1412"/>
        <source>Remove Bookmark</source>
        <translation>Elimina semn de carte</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1505"/>
        <source>Flags</source>
        <translation>Atentionari</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1557"/>
        <source>Graphs</source>
        <translation>Grafice</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1582"/>
        <source>Show/hide available graphs.</source>
        <translation>Arata/ascunde graficele disponibile.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="283"/>
        <source>Breakdown</source>
        <translation>Detaliere</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="283"/>
        <source>events</source>
        <translation>evenimente</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="294"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="295"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="358"/>
        <source>Time at Pressure</source>
        <translation>Timp la Presiunea</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="602"/>
        <source>No %1 events are recorded this day</source>
        <translation>Niciun eveniment %1 nu a fost inregistrat in aceasta zi</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="722"/>
        <source>%1 event</source>
        <translation>%1 eveniment</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="723"/>
        <source>%1 events</source>
        <translation>%1 evenimente</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="766"/>
        <source>Session Start Times</source>
        <translation>Inceputul Sesiunii</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="767"/>
        <source>Session End Times</source>
        <translation>Sfarsitul Sesiunii</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="973"/>
        <source>Session Information</source>
        <translation>Informatii despre Sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="996"/>
        <source>Oximetry Sessions</source>
        <translation>Sesiuni pulsoximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1016"/>
        <source>Duration</source>
        <translation>Durata</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2278"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="993"/>
        <source>CPAP Sessions</source>
        <translation>Sesiuni CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="178"/>
        <source>Details</source>
        <translation>Detalii</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="999"/>
        <source>Sleep Stage Sessions</source>
        <translation>Sesiuni Etape Somn</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1002"/>
        <source>Position Sensor Sessions</source>
        <translatorcomment>/Pozitionati Sesiunile Senzorului</translatorcomment>
        <translation>Inregistrari ale senzorului de pozitie</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1007"/>
        <source>Unknown Session</source>
        <translation>Sesiune necunoscuta</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1077"/>
        <source>Machine Settings</source>
        <translation>Setari aparat</translation>
    </message>
    <message>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing&apos;s changed since previous days.</source>
        <translation type="vanished">&lt;b&gt;Atentie:&lt;/b&gt; Toate setarile de mai jos presupun ca nimic nu s-a schimbat fata de zilele precedente.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1083"/>
        <source>Machine Settings Unavailable</source>
        <translation>Setari aparat indisponibile</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1230"/>
        <source>Model %1 - %2</source>
        <translation>Model %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1235"/>
        <source>PAP Mode: %1</source>
        <translation>PAP Mod: %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1239"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation>(Mod/Setari presiune sunt intuite in aceasta zi)</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1350"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Aceasta zi contine doar date sumare, datele disponibiea sunt  limitate.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1376"/>
        <source>Total ramp time</source>
        <translation>Timp total in RAMP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1380"/>
        <source>Time outside of ramp</source>
        <translation>Timp in afara RAMP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1421"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1421"/>
        <source>End</source>
        <translation>Sfarsit</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1458"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation>Nu pot afisa graficul PieChart pe acest sistem</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1697"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation>Regret, acest aparat CPAP furnizeaza doar date despre complianta (adica in ce masura respectati indicatiile medicului inregistrate in aparat).</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1716"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Nu e nimic aici!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1719"/>
        <source>No data is available for this day.</source>
        <translation>NU exista date pentru aceasta zi.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1204"/>
        <source>Oximeter Information</source>
        <translation>Informatii Pulsoximetru</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1034"/>
        <source>Click to %1 this session.</source>
        <translation>Click pentru a %1 acesta sesiune.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1034"/>
        <source>disable</source>
        <translation>dezactiveaza</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1034"/>
        <source>enable</source>
        <translation>activeaza</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1050"/>
        <source>%1 Session #%2</source>
        <translation>%1 Sesiune #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1051"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1065"/>
        <source>One or more waveform record(s) for this session had faulty source data. Some waveform overlay points may not match up correctly.</source>
        <translation>Una sau mai multi parametri inregistrati in aceasta zi au date eronate. Unele valori ale graficelor nu se vor putea corela cum trebuie.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1081"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1208"/>
        <source>SpO2 Desaturations</source>
        <translation>SpO2 desaturata</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1209"/>
        <source>Pulse Change events</source>
        <translation>Evenimente ale Pulsului</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1210"/>
        <source>SpO2 Baseline Used</source>
        <translation>SpO2 de baza</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1281"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1276"/>
        <source>Statistics</source>
        <translation>Statistici</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1360"/>
        <source>Total time in apnea</source>
        <translation>Timp total in apnee</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1370"/>
        <source>Time over leak redline</source>
        <translation>Timp de scurgere pe langa masca peste limita admisa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1635"/>
        <source>BRICK! :(</source>
        <translation>BRICK! :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1445"/>
        <source>Event Breakdown</source>
        <translation>Detaliere Eveniment</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1687"/>
        <source>Sessions all off!</source>
        <translation>Toate Sesiunile dezactivate!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1689"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Eexista Sesiuni in aceasta zi dar afisarea lor e dezactivata.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1692"/>
        <source>Impossibly short session</source>
        <translation>Sesiune mult prea scurta</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1693"/>
        <source>Zero hours??</source>
        <translation>Zero ore??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1696"/>
        <source>BRICK :(</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1698"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Reclamati aceasta furnizorului dvs de CPAP!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2014"/>
        <source>Pick a Colour</source>
        <translation>Alegeti o culoare</translation>
    </message>
    <message>
        <source>This bookmarked is in a currently disabled area..</source>
        <translation type="vanished">Acest semn de carte este intr-o zona momentan dezactivata..</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2321"/>
        <source>Bookmark at %1</source>
        <translation>Semne de carte la %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Exporta ca fisier CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Datele:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Concluzie:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Detalii</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sesiuni</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Zilnic</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Nume ficier:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Anuleaza</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Exporta</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Sfarsit:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Interval rapid:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="60"/>
        <location filename="../oscar/exportcsv.cpp" line="122"/>
        <source>Most Recent Day</source>
        <translation>Cea mai recenta zi</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="125"/>
        <source>Last Week</source>
        <translation>Saptamana trecuta</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="128"/>
        <source>Last Fortnight</source>
        <translatorcomment>/Ultimele 14 zile</translatorcomment>
        <translation>Ultimele doua saptamani</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="131"/>
        <source>Last Month</source>
        <translation>Ultima luna</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="134"/>
        <source>Last 6 Months</source>
        <translation>Ultimele 6 luni</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="137"/>
        <source>Last Year</source>
        <translation>Anul trecut</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="119"/>
        <source>Everything</source>
        <translation>Totul</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="108"/>
        <source>Custom</source>
        <translation>Personalizat</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="73"/>
        <source>OSCAR_</source>
        <translation>OSCAR_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="76"/>
        <source>Details_</source>
        <translation>Detalii_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="78"/>
        <source>Sessions_</source>
        <translation>Sesiuni_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="80"/>
        <source>Summary_</source>
        <translation>Sumar_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="87"/>
        <source>Select file to export to</source>
        <translation>Selectati fisierul in care sa exportez</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>CSV Files (*.csv)</source>
        <translation>CSV Files (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>Sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>Eveniment</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>Date/Durata</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>Numar Sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>Sfarsit</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>Timp Total</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> Numar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="222"/>
        <source>%1% </source>
        <translation>%1% </translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation>Eroare la Importare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation>Datele din acest aparat nu pot fi importate in acest profil.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Datele din aceasta zi se suprapun cu cele deja existente.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation>Ascunde acest mesaj</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation>Cauta subiect:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Fisierele de Help (ajutor) nu sunt inca disponibile pentru %1 si vor fi afisate in %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation>Fisierele Help (Ajutor) nu par sa fie prezente.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>HelpEngine (subrutina de ajutor) nu este setata corect</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>HelpEngine (subrutina de ajutor) nu a putut deschide corect documentatia.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation>Continut</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation>Index</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation>Cauta</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation>Nu exista documentatie disponibila</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation>Va rog asteptati putin.. Indexarea este in curs de finalizare</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation>Nu</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 resultat(e) pentru &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translation>curata</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation>Nu am gasit fisierul cu oximetria:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation>Nu am putut deschide fisierul cu oximetria:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation>&amp;Statistici</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation>Modul de Raportare</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <location filename="../oscar/mainwindow.ui" line="3243"/>
        <source>Standard</source>
        <translation>Standard</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation>Lunar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation>Interval Date</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation>Statistici</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation>Zilnic</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation>Imagine de ansamblu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <source>Oximetry</source>
        <translation>Pulsoximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation>Importa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation>Ajutor</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation>&amp;Vizualizare</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2834"/>
        <source>&amp;Reset Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2857"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajutor</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2874"/>
        <source>&amp;Data</source>
        <translation>&amp;Date</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2878"/>
        <source>&amp;Advanced</source>
        <translation>&amp;Avansat</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2898"/>
        <source>Rebuild CPAP Data</source>
        <translation>Reconstruieste Datele CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2945"/>
        <source>Show Daily view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>Show Overview view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2996"/>
        <source>&amp;Maximize Toggle</source>
        <translation>&amp;Maximizeaza fereastra</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2999"/>
        <source>Maximize window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3018"/>
        <source>Reset Graph &amp;Heights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3021"/>
        <source>Reset sizes of graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3086"/>
        <source>Show Right Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3100"/>
        <source>Show Statistics view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3146"/>
        <source>Show &amp;Line Cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3168"/>
        <source>Show Daily Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3185"/>
        <source>Show Daily Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3216"/>
        <source>Report an Issue</source>
        <translation>Raporteaza o problema</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3221"/>
        <source>System Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3232"/>
        <source>Show &amp;Pie Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3235"/>
        <source>Show Pie Chart on Daily page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3238"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3246"/>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3251"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3254"/>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2927"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Preferinte</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2932"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Profile</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2988"/>
        <source>&amp;About OSCAR</source>
        <translation>&amp;Despre OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3201"/>
        <source>Show Performance Information</source>
        <translation>Arata informatii despre performanta</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3206"/>
        <source>CSV Export Wizard</source>
        <translation>CSV Export semiautomat</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3211"/>
        <source>Export for Review</source>
        <translation>Exporta pentru evaluare</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>E&amp;xit</source>
        <translation>E&amp;xit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2937"/>
        <source>Exit</source>
        <translation>Exit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2942"/>
        <source>View &amp;Daily</source>
        <translation>Vizualizare &amp;Zilnica</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2953"/>
        <source>View &amp;Overview</source>
        <translation>Vizualizare de &amp;Ansamblu</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2964"/>
        <source>View &amp;Welcome</source>
        <translation>Vizualizare &amp;Prima pagina</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2972"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2983"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Folositi &amp;AntiAliasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3010"/>
        <source>Show Debug Pane</source>
        <translation>Aratati panelul de depanare</translation>
    </message>
    <message>
        <source>&amp;Reset Graph Layout</source>
        <translation type="vanished">&amp;Reseteaza aspectul graficului</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3026"/>
        <source>Take &amp;Screenshot</source>
        <translation>Capturati &amp;Ecran</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3034"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>Pulso&amp;ximetrie semiautomata</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3042"/>
        <source>Print &amp;Report</source>
        <translation>Tipareste &amp;Raportul</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3047"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editeaza Profil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3182"/>
        <source>Daily Calendar</source>
        <translation>CAlendar Zilnic</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3193"/>
        <source>Backup &amp;Journal</source>
        <translation>Backup &amp;Jurnal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3052"/>
        <source>Online Users &amp;Guide</source>
        <translation>&amp;Ghid utilizator Online (EN)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3057"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>&amp;Intrebari frecvente</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3062"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>&amp;Curatare automata pusoximetrie</translation>
    </message>
    <message>
        <source>Toggle &amp;Line Cursor</source>
        <translation type="vanished">Modifica Cursorul &amp;Liniei</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3067"/>
        <source>Change &amp;User</source>
        <translation>Schimba &amp;Utilizator</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3072"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Elimina Ziua &amp;Curenta selectata</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3083"/>
        <source>Right &amp;Sidebar</source>
        <translation>Bara de unelte din &amp;dreapta</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3165"/>
        <source>Daily Sidebar</source>
        <translation>Bara de activitati Zilnica</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3094"/>
        <source>View S&amp;tatistics</source>
        <translation>Vizualizare S&amp;tatistici</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation>Navigare</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation>Semne de carte</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation>Inregistrari</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2888"/>
        <source>Purge ALL CPAP Data</source>
        <translation>Elimina toate Datele CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation>Exp&amp;orta Date</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation>Profile</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2882"/>
        <source>Purge Oximetry Data</source>
        <translation>Elimina Datele de Pulsoximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2919"/>
        <source>&amp;Import SDcard Data</source>
        <translation>&amp;Importa Date din  SDcard</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3097"/>
        <source>View Statistics</source>
        <translation>Vizualizare Statistici</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Importa Date &amp;ZEO</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3113"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importa Date din  RemStar &amp;MSeries</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3118"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>&amp;Glosar de termeni despre Apneea de somn</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3123"/>
        <source>Change &amp;Language</source>
        <translation>Schimba &amp;Limba</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3128"/>
        <source>Change &amp;Data Folder</source>
        <translation>Schimba &amp;Data Folder</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3133"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importa Date din &amp;Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3138"/>
        <source>Current Days</source>
        <translation>Zilele curente</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="530"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="509"/>
        <location filename="../oscar/mainwindow.cpp" line="2236"/>
        <source>Welcome</source>
        <translation>Bun venit</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="100"/>
        <source>&amp;About</source>
        <translation>&amp;Despre</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="715"/>
        <location filename="../oscar/mainwindow.cpp" line="1926"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Va rog asteptati, impotez din backup...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="660"/>
        <source>Import Problem</source>
        <translation>Problema la importare</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="816"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Introduceti cardul cu datele dvs CPAP (vedeti sa fie Read-Only!)...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="889"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Importul a fost dezactivat cat timp are loc reanaliza datelor.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="928"/>
        <source>CPAP Data Located</source>
        <translation>DAte CPAP localizate</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="957"/>
        <source>Please remember to point the importer at the root folder or drive letter of your data-card, and not a subfolder.</source>
        <translation>Directionati Importatorul catre radacina cardului dvs si nu catre un subdirector.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="958"/>
        <source>Import Reminder</source>
        <translation>Reamintire Importare</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1021"/>
        <source>Processing import list...</source>
        <translation>Procesez lista imortata...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1042"/>
        <source>Importing Data</source>
        <translation>Import Datele</translation>
    </message>
    <message>
        <source>This software has been created to assist you in reviewing the data produced by CPAP Machines, used in the treatment of various Sleep Disorders.</source>
        <translation type="vanished">Acest software a fost creat ca sa va asiste in vizualizarea datelor inregistrate de anumite aparate CPAP cu memorie, utilizate in tratamentul variatelor forme de apnee de somn.</translation>
    </message>
    <message>
        <source>This is a beta release, some features may not yet behave as expected.</source>
        <translation type="vanished">Aceasta este o versiune Beta, anumite componente e posibil sa nu se comporte cum va asteptati.</translation>
    </message>
    <message>
        <source>Currenly supported machines:</source>
        <translation type="vanished">Aparate compatibile momentan:</translation>
    </message>
    <message>
        <source>CPAP</source>
        <translation type="vanished">CPAP</translation>
    </message>
    <message>
        <source>Philips Respironics System One (CPAP Pro, Auto, BiPAP &amp; ASV models)</source>
        <translation type="vanished">Philips Respironics System One (CPAP Pro, Auto, BiPAP &amp; ASV models)</translation>
    </message>
    <message>
        <source>ResMed S9 models (CPAP, Auto, VPAP)</source>
        <translation type="vanished">ResMed S9 models (CPAP, Auto, VPAP)</translation>
    </message>
    <message>
        <source>DeVilbiss Intellipap (Auto)</source>
        <translation type="vanished">DeVilbiss Intellipap (Auto)</translation>
    </message>
    <message>
        <source>Fisher &amp; Paykel ICON (CPAP, Auto)</source>
        <translation type="vanished">Fisher &amp; Paykel ICON (CPAP, Auto)</translation>
    </message>
    <message>
        <source>Contec CMS50D+, CMS50E and CMS50F (not 50FW) Oximeters</source>
        <translation type="vanished">PulsOximetere Contec CMS50D+, CMS50E and CMS50F (nu si 50FW)</translation>
    </message>
    <message>
        <source>ResMed S9 Oximeter Attachment</source>
        <translation type="vanished">Accesoriu Oximetru la ResMed S9</translation>
    </message>
    <message>
        <source>Online Help Resources</source>
        <translation type="vanished">Resurse Help (Ajutor, EN) online</translation>
    </message>
    <message>
        <source>Note:</source>
        <translation type="vanished">Nota:</translation>
    </message>
    <message>
        <source>I don&apos;t recommend using this built in web browser to do any major surfing in, it will work, but it&apos;s mainly meant as a help browser.</source>
        <translation type="vanished">Nu recomand aceasta versiune pentru a naviga pe internet, va merge, dar nu e menit pentru a naviga sigur pe net.</translation>
    </message>
    <message>
        <source>(It doesn&apos;t support SSL encryption, so it&apos;s not a good idea to type your passwords or personal details anywhere.)</source>
        <translation type="vanished">(programul nu suporta criptare SSl, deci e bine sa nu scrii parole pe aici)</translation>
    </message>
    <message>
        <source>Further Information</source>
        <translation type="vanished">Mai multe informatii</translation>
    </message>
    <message>
        <source>Plus a few &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;usage notes&lt;/a&gt;, and some important information for Mac users.</source>
        <translation type="vanished">Plus cateva &lt;a href=&apos;qrc:/docs/usage.html&apos;&gt;note despre utilizare&lt;/a&gt;, si informatii importante pentru utilizatorii de MacIntosh.</translation>
    </message>
    <message>
        <source>About &lt;a href=&apos;http://en.wikipedia.org/wiki/Sleep_apnea&apos;&gt;Sleep Apnea&lt;/a&gt; on Wikipedia</source>
        <translation type="vanished">Despre &lt;a href=&apos;http://en.wikipedia.org/wiki/Sleep_apnea&apos;&gt;Sleep Apnoea&lt;/a&gt; pe Wikipedia (EN)</translation>
    </message>
    <message>
        <source>Friendly forums to talk and learn about Sleep Apnea:</source>
        <translation type="vanished">Un Forum prietenos unde puteti vorbi despre apnee:</translation>
    </message>
    <message>
        <source>&lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;CPAPTalk Forum&lt;/a&gt;,</source>
        <translation type="vanished">&lt;a href=&apos;http://www.cpaptalk.com&apos;&gt;CPAPTalk Forum&lt;/a&gt; (EN),</translation>
    </message>
    <message>
        <source>Copyright:</source>
        <translation type="vanished">Copyright:</translation>
    </message>
    <message>
        <source>License:</source>
        <translation type="vanished">Licenta:</translation>
    </message>
    <message>
        <source>DISCLAIMER:</source>
        <translation type="vanished">DISCLAIMER:</translation>
    </message>
    <message>
        <source>This is &lt;font color=&apos;red&apos;&gt;&lt;u&gt;NOT&lt;/u&gt;&lt;/font&gt; medical software. This application is merely a data viewer, and no guarantee is made regarding accuracy or correctness of any calculations or data displayed.</source>
        <translation type="vanished">Acesta &lt;font color=&apos;red&apos;&gt;&lt;u&gt;NU&lt;/u&gt;&lt;/font&gt; este un software medical. Aceasta aplicatie open-source este doar un vizualizator de date, nu exista nicio garantie in ceea ce priveste acuratetea sau corectitudinea datelor afisate.</translation>
    </message>
    <message>
        <source>Your doctor should always be your first and best source of guidance regarding the important matter of managing your health.</source>
        <translation type="vanished">Doctorul dvs trebuie sa fie principalul dvs ghid in ingrijirea sanatatii ca si in tratamentul apnnei de somn.</translation>
    </message>
    <message>
        <source>*** &lt;u&gt;Use at your own risk&lt;/u&gt; ***</source>
        <translation type="vanished">*** &lt;u&gt;Utilizati acest program pe riscul dvs&lt;/u&gt; ***</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1392"/>
        <source>Updates are not yet implemented</source>
        <translation>Actualizarile nu sunt inca implementate</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1505"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation>Ghidul de utilizare se va deschide in browser-ul dvs de internet</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1513"/>
        <source>The FAQ is not yet implemented</source>
        <translation>Sectiune Intrebari frecvente nu este inca implementata</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1761"/>
        <location filename="../oscar/mainwindow.cpp" line="1788"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation>Daca puteti citi asta, inseamna ca nu a functionat repornirea. Va trebui sa reporniti manual.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1901"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation>Sunteti sigur ca doriti sa reconstruiti toate datele CPAP pentru aparatul urmator:

</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1911"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation>Dintr-un oarecare motiv OSCAR nu are un backup pentru aparatul:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1966"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation>Sunteti pe cale sa &lt;font size=+2&gt;eliminati&lt;/font&gt; baza de date a OSCAR pentru acest aparat CPAP:&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2023"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation>O eroare de permisiuni ale fisierelor a sabotat procesul de curatire; va trebui sa stergeti manual acest dosar:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2073"/>
        <source>No help is available.</source>
        <translation>Nu exista Help (Ajutor) disponibil.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2317"/>
        <source>Donations are not implemented</source>
        <translation>Donatiile nu sunt inca implementate aici, mergeti pe &lt;a href=&apos;https://www.apneaboard.com/donate.htm&apos;&gt;site&lt;/a&gt; (EN)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2527"/>
        <source>%1&apos;s Journal</source>
        <translation>Jurnalul lui %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2529"/>
        <source>Choose where to save journal</source>
        <translation>Alegeti unde salvez jurnalul</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2529"/>
        <source>XML Files (*.xml)</source>
        <translation>XML Files (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2549"/>
        <source>Export review is not yet implemented</source>
        <translation>Exportul sumarului nu este inca implementat</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2564"/>
        <source>Reporting issues is not yet implemented</source>
        <translation>Raportarea problemelor online nu este inca implementata</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2573"/>
        <source>OSCAR Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="245"/>
        <source>Help Browser</source>
        <translation>Vizualizare Help (Ajutor)</translation>
    </message>
    <message>
        <source>Welcome to OSCAR</source>
        <translation type="vanished">Bun venit la OSCAR</translation>
    </message>
    <message>
        <source>About OSCAR</source>
        <translation type="vanished">Despre OSCAR</translation>
    </message>
    <message>
        <source>OSCAR has been designed by a software developer with personal experience with a sleep disorder, and shaped by the feedback of many other willing testers dealing with similar conditions.</source>
        <translation type="vanished">OSCAR a fost conceput de un programator cu apnee si apoi completat de multi altii care s-au confruntat cu probleme la care au gasit solutii.</translation>
    </message>
    <message>
        <source>Please report any bugs you find to the OSCAR developer&apos;s group.</source>
        <translation type="vanished">Va rog raportati orice problema cu OSCAR dezvoltatorilor, pe forum.</translation>
    </message>
    <message>
        <source>This software is released freely under the &lt;a href=&quot;qrc:/COPYING&quot;&gt;GNU Public License version 3&lt;/a&gt;.</source>
        <translation type="vanished">Acest software este gratuit, sub licenta &lt;a href=&quot;qrc:/COPYING&quot;&gt;GNU Public License version 3&lt;/a&gt;.</translation>
    </message>
    <message>
        <source>The authors will NOT be held liable by anyone who harms themselves or others by use or misuse of this software.</source>
        <translation type="vanished">Autorii nu pot fi trasi la raspundere daca va faceti rau dvs sau altora prin folosirea sau nefolosirea acestui software.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1331"/>
        <source>Please open a profile first.</source>
        <translation>Va rugam deschideti mai intai un Profil.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1913"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Daca v-ati facut &lt;i&gt;propriile &lt;b&gt;backup-uri&lt;/b&gt; la toate datele CPAP&lt;/i&gt;, puteti finaliza aceasta operatiune, dar va trebui sa le restaurati la nevoie din backup manual.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1914"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Sunteti sigur ca doriti ca asta doriti sa faceti?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1929"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Deoarece nu exista backup-uri interne pentru a reface datele, va trebui sa faceti restaurarea manuala a lor.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1930"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation>Doriti sa importati din backup-ul dvs? (nu sunt Date pentru acest aparat CPAP pana nu faceti acest lucru)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1968"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Nota: Ca precautie, dosarul de backup va fi lasat la locul lui.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1969"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Sunteti &lt;b&gt;absolut sigur&lt;/b&gt; ca doriti ca continuati?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2309"/>
        <source>The Glossary will open in your default browser</source>
        <translation>Glosarul de termeni se va deschide in browser-ul dvs de internet</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2457"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Sunteti sigur ca vreti sa stergeti datele pulsoximetriei pentru %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2459"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Atentie, nu veti mai putea reveni asupra acestei operatiuni!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2480"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Selectati mai intai ziua cu Date valide de Pulsoximetrie in Fereasta de vizualizare a zilei.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="320"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="491"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Incarc profilul &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="656"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>Am Importat %1 sesiuni CPAP din

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="656"/>
        <source>Import Success</source>
        <translation>Importul s-a finalizat cu succes</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="658"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation>Datele actualizate deja cu aparatul CPAP la

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="658"/>
        <source>Up to date</source>
        <translation>La zi</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="660"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation>Nu am gasit date valide la 

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="823"/>
        <source>Choose a folder</source>
        <translation>Alegeti un dosar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="885"/>
        <source>No profile has been selected for Import.</source>
        <translation>Nu a fost selectat niciun Profil pentru Import.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="893"/>
        <source>Import is already running in the background.</source>
        <translation>Importarea ruleaza inca in fundal.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="921"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>O structura %1 a fisierului pentru %2 a fost localizata la:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="923"/>
        <source>A %1 file structure was located at:</source>
        <translation>Un fisier %1 a fost localizat la:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="929"/>
        <source>Would you like to import from this location?</source>
        <translation>Doriti sa importati din aceasta locatie?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="932"/>
        <source>Specify</source>
        <translation>Specificati</translation>
    </message>
    <message>
        <source>The release notes for this version can be found in the About OSCAR menu item.</source>
        <translation type="vanished">Notele despre aceasta versiune pot fi gasite in meniul Despre OSCAR.</translation>
    </message>
    <message>
        <source>&lt;a href=&apos;http://www.apneaboard.com/forums/&apos;&gt;Apnea Board&lt;/a&gt;</source>
        <translation type="vanished">&lt;a href=&apos;http://www.apneaboard.com/forums/&apos;&gt;Apnea Board&lt;/a&gt; (EN)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1336"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Preferintele au fost dezactivate cat timp are loc reanaliza datelor.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1442"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>A aparut o problema la salvarea capturii in fisierul &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1444"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Captura ecran salvata in fisierul &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1760"/>
        <location filename="../oscar/mainwindow.cpp" line="1787"/>
        <source>Gah!</source>
        <translation>Ah!</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1904"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation>Atentie, puteti pierde datele daca backup-ul OSCAR a fost dezactivat.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2268"/>
        <source>There was a problem opening ZEO File: </source>
        <translation>A aparut o problema la deschiderea fisierului din aparatul ZEO: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2272"/>
        <source>Zeo CSV Import complete</source>
        <translation>Importul CSV din ZEO s-a finalizat</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2294"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>A aparut o problema la deschiderea fisierului din aparatul MSeries: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2298"/>
        <source>MSeries Import complete</source>
        <translation>Importul finalizat din aparatul MSeries</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2355"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation>A aparut o problema la deschiderea fisierului din aparatul Somnopose: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2359"/>
        <source>Somnopause Data Import complete</source>
        <translation>Datele Somnopause au fost importate</translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2019"/>
        <source>Auto-Fit</source>
        <translation>Potrivire automata (Auto-Fit)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Defaults</source>
        <translation>Setari initiale</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2021"/>
        <source>Override</source>
        <translation>Suprascrie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2022"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>Reglarea pe axa Y, &apos;Auto-Fit pentru potrivire automata in ecran, &apos;Setari initiale&apos; pentru setarile initiale a le OSCAR, si &apos;Override&apos; ca sa alegeti alte setari, proprii.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2028"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>Valoarea maxima pe axa Y.. poate fi un numar negativ daca doriti.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2029"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>Valoarea maxima pe axa Y.. trebuie sa fie mai mare decat Minimul.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2064"/>
        <source>Scaling Mode</source>
        <translation>Scaling Mode</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2086"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Acest buton reseteaza MIn si Max ca sa se potriveasca cu Auto-Fit</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Editeaza profil utilizator</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation>Accept si imi asum toate conditiile de mai sus.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation>Informatii utilizator</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation>Numele utilizatorului</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation>Nu lasati a indemana copiilor.. Nimic altceva.. Programul nu e super-securizat si puteti avea probleme MARI.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation>Parola de protectie a Profilului</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation>Parola</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation>...de doua ori...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation>Localizare (Locale)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation>Tara</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation>Zona orara</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation>DST Zone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation>Informatii personale pentru Raportari</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation>Prenume</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation>Nume</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Puteti sari peste astea, dar varsta este necesara pentru a imbunatati acuratetea unor calcule.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation>D.O.B.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sexul biologic este necesar uneori pentru a imbunatati acuratetea unor calcule,puteti lasa liber daca doriti.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation>Sex</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation>Barbat</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation>Femeie</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation>Inaltime</translation>
    </message>
    <message>
        <source>metric</source>
        <translation type="vanished">metric</translation>
    </message>
    <message>
        <source>archiac</source>
        <translation type="vanished">arhaic</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation>Informatii Contact</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation>Informatii tratament CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation>Data diagnostcului</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation>AHI (apneea-hipopneea index) netratata</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation>Mod CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation>Presiuni recomandate de medic</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation>Informatii Doctori / Clinica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation>Numele doctorului</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation>Numele clinicii</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation>ID pacient</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuleaza</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation>&amp;Inapoi</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <location filename="../oscar/newprofile.cpp" line="283"/>
        <source>&amp;Next</source>
        <translation>&amp;Urmatorul</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="61"/>
        <source>Select Country</source>
        <translation>Alegeti tara</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="109"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Bun venit la  Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation>Acest software este creat ca sa va asiste in vizualizarea datelor inregistrate de anumite aparate CPAP si echipamente inrudite.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>CITITI CU ATENTIE</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="120"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>Acuratetea datelor afisate nu poate fi garantata.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Orice rapoarte generate sunt pentru UZ PERSONAL si NICINTR-UN CAZ nu sunt potrivite pentru complianta sau pentru vreun diagnostic medical.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="129"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Utilizati acest software pe propriul dvs risc.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="114"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation>OSCAR este gratuit sub licenta &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, si nu are nicio garantie in ceea ce priveste scopul sau acuratetea informatiilor.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="117"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation>OSCAR este destinat doar a fi un vizualizator de date, și cu siguranță nu este un substitut pentru îndrumarea medicală competentă de la medicul dumneavoastră.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="125"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation>Autorii nu vor fi responsabili de &lt;u&gt;nimic&lt;/u&gt;in legatura cu utilizarea sau neutilizarea acestui software. Asta e free GPU license.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="132"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation>OSCAR are copyright &amp;copy;2011-2018 Mark Watkins &amp; unele portiuni &amp;copy;2019 Nightowl Software</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="155"/>
        <source>Please provide a username for this profile</source>
        <translation>Va rugam furnizati numele de utilizator pentru acest profil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="165"/>
        <source>Passwords don&apos;t match</source>
        <translation>Parola introdusa nu e  identica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Profile Changes</source>
        <translation>Schimbari profil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Accept and save this information?</source>
        <translation>Accepta si salveaza aceste informatii?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="272"/>
        <source>&amp;Finish</source>
        <translation>&amp;Finalizeaza</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="448"/>
        <source>&amp;Close this window</source>
        <translation>&amp;Inchide aceasta fereastra</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Interval:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Ultima saptamana</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Ultimele doua saptamani</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Ultima luna</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Ultimele doua luni</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Ultimele trei luni</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Ultimele 6 luni</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Ultimul an</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Tot</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Particularizat</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation>Start:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation>Sfarsit:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translation>Resetati vizualizarea la intervalul de timp selectat</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation>Modifica vizibilitatea graficelor</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Alegeti graficele pe care doriti sa le dez/activati.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="267"/>
        <source>Graphs</source>
        <translation>Grafice</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="183"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Indicele de Afectare Respiratorie</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="185"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Apnoea
Hypopnea
Index</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="191"/>
        <source>Usage</source>
        <translation>Utilizare</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="191"/>
        <source>Usage
(hours)</source>
        <translation>Utilizare
(ore)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="194"/>
        <source>Session Times</source>
        <translation>Timip Sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="202"/>
        <source>Total Time in Apnea</source>
        <translation>Timp Total in Apnee</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="202"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Timp Total in Apneea
(Minute)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="237"/>
        <source>Body
Mass
Index</source>
        <translation>Indice
de Masa
Corporala</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="238"/>
        <source>How you felt
(0-10)</source>
        <translation>Cum v-ati simtitt
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="576"/>
        <source>Show all graphs</source>
        <translation>Arata toate graficele</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="589"/>
        <source>Hide all graphs</source>
        <translation>Ascunde toate graficele</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation>Import semiautomat din Pulsoximetru</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation>Sari peste aceasta pagina data viitoare.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation>De unde doriti sa importati?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation>Utilizatorii de pulsoximetre CMS50E/F cand importati direct, nu confirmati pe ecranul pulsoximetrului pana cand OSCAR nu va spune sa faceti acest lucru.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Daca e bifat, OSACR va reseta automat ceasul pulsoximetrului Contec CMS50 folosind ora computerului.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  becauseif something goes wrong before OSCAR saves your session, you won&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Stergeti sesiunea importata din pulsoximetru dupa finalizarea descarcarii pe computer. &lt;/p&gt;&lt;p&gt;Folositi cu atentie, ca daca ceva nu merge bine inainte ca OSCAR sa poata salva sesiune, nu mai puteti recupera datele respective.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, some oximeters will require you to do something in the devices menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Importati (prin cablu) inregistrarile din pulsoximetrul dvs.&lt;/p&gt;&lt;p&gt;Dupa ce ati selectat aceasta optiune, unele pulsoximetre va vor cere sa faceti ceva in meniul lor ca sa initiati incarcarea pe computer.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Daca nu va deranjeaza sa fiti agatat de un computer toata noaptea, acesta optiune creaza un grafic pletismografic util, care indica si ritmul cardiac, in afara de valorile oximetriei.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Inregistreaza atasat de computer toata noapte (se obtine o pletismograma)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Importati datele dintr-un fisier creat pe computer de software-ul pulsoximetrului dvs cum ar fi SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Importa dintr-un fisier de date salvat de un alt program cum este SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <source>Please connect your oximeter device</source>
        <translation>Conectati pulsoximetrul</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Daca cititi asta, cel mai probabil ati setat gresit tipul pulsoximetrului in preferinte.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation>Apasati START pentru a incepe inregistrarea</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation>Arata grafice in timp real</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation>Durata</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation>Pulsul</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation>Au fost detectate mai multe sesiuni</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation>Timp de inceput</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation>Detalii</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Importul s-a finalizat cu succes. Cand a inceput inregistrarea?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translation>Inregistrarea zilnica a inceput</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation>Ora inceperii oximetriei</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>reau sa folosesc ora raportata de ceasul intern al pulsoximetrului.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation>Am pornit aceasta sesiune de oximetrie la (sau aproape de) acelasi moment in care am pornit CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Atentie: sincronizarea sesiunii de oximetrie cu inceputul sesiunii CPAP va fi intotdeauna mai exacta.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Alegeti sesiunea CPAP cu care sa fac sincronizarea oximetriei:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>Puteti ajusta manual ora aici daca e nevoie:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ssap</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuleaza</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation>Pagina cu &amp;Informatii</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Atentie: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Asigurati-va ca ati selectat corect modelul pulsoximetrului, altfel importul va esua.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation>Selectati tipul pulsoximetrului:</translation>
    </message>
    <message>
        <source>CMS50Fv3.7+/H/I, Pulox PO-400/500</source>
        <translation type="vanished">CMS50Fv3.7+/H/I, Pulox PO-400/500</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation>CMS50D+/E/F, Pulox PO-200/300</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation>ChoiceMMed MD300W1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation>Setati data/timpul dispozitivului</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Setati pentru a activa identificatorul dispozitivului la urmatorul Import de date, util pentru cei care au mai multe pulsoximetre.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation>Setati identificatorul dispozitivului</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character pet name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Introduceti un nume de 7 caractere pentru acest pulsoximetru.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation>Sterge sesiunea din aparat dupa ce a fost incarcata in OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation>Importa direct dintr-un dispozitiv</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Atentie utilizatorilor de CPAP: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Ati importat mai intai sesiunile CPAP?&lt;br/&gt;&lt;/span&gt;Daca ati uitat, nu veti avea o linie temporala cu care sa va sincronizati inregistrarile pulsoximetrului, care nu are ceas  propriu.&lt;br/&gt;Pentru a realiza sincronizarea intre dispozitive, incercati intotdeauna sa incepeti inregistrarile la aceeasi ora.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation>Alegeti fiserul pe care vreti sa-l importati in OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR are nevoie de o ora de inceput pentru a sti unde sa sincronizeze aceasta sesiune de oximetrie.&lt;/p&gt;&lt;p&gt;Alegeti din urmatoarele optiuni:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation>&amp;Reincearca</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation>&amp;Alege sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation>&amp;Finalul inregistrarii</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Sincronizeaza si Salveaza</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Salveaza si incheie</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Caut pulsoximetre compatibile</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Nu am putut detecta niciun pulsoximetru conectat.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Conectare la pulsoximetrul %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Redenumesc acest pulsoximetru de la &apos;%1&apos; la &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>Numele pulsoximetrului este diferit.. Daca aveti doar unul pe care il folositi in mai multe profile, setati acelasi nume in ambele profile OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="302"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, sesiune %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Nothing to import</source>
        <translation>Nu e nimic de importat</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Pulsoximetrul dvs nu a salvat nicio sesiune valida de oximetrie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Close</source>
        <translation>Inchide</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="332"/>
        <source>Waiting for %1 to start</source>
        <translation>Astept sa porneasca %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Astept ca dispozitivul sa porneasca procesul de incarcare a datelor...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Select upload option on %1</source>
        <translation>Selectati optiunea de incarcare pe %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="336"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>Din meniul pulsoximetrului, incepeti rrimiterea datelor catre computer.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Conectati pulsoximetrul, intrati in meniul lui si selectati &quot;Upload&quot; pentru a incepe transferul datelor...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="367"/>
        <source>%1 device is uploading data...</source>
        <translation>%1 incarca datele...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="368"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Asteptati pana cand se termina procesul de incarcare a datelor in OSCAR. Nu deconectati pulsoximetrul.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="387"/>
        <source>Oximeter import completed..</source>
        <translation>Importul din pulsoximetru s-a finalizat cu succes.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Select a valid oximetry data file</source>
        <translation>Selectati un fisier de oximetrie compatibil</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Fisiere Oximetrie (*.spo *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="433"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Niciun modul de oximetrie nu a putut descifra fisierul descarcat:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Live Oximetry Mode</source>
        <translation>Mod Oximetrie direct</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="530"/>
        <source>Live Oximetry Stopped</source>
        <translation>Mod Oximetrie direct oprit</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="531"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>Importul direct al oximetriei fost oprit</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1091"/>
        <source>Oximeter Session %1</source>
        <translation>Sesiunea de oximetrie %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1136"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation>OSCAR va da posibilitatea sa urmariti datele oximetriei concomitent cu cele ale CPAP pentru a obtine informatii relevante despre eficienta tratamentului CPAP. Deasemenea, poate vizualiza si separat datele din Pulsoximetre, permitand inregistrarea si analiza ulterioara a datelor.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1147"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Daca incercati sa sincronizati datele din oximetrie si CPAP, asigurati-va ca ati importat INTAI datele din CPAP si abia apoi pe cele din Pulsoximetru!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1150"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation>Pentru ca OSCAR sa poata localiza si citi direct din pulsoximetrul dvs, asigurati-va ca aveti instalate driverele potrivite (ex. USB to Serial UART) pe computer. Pentru mai multe informatii %1click aici %2.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="460"/>
        <source>Oximeter not detected</source>
        <translation>Nu ati selectat pulsoximetrul</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="467"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Nu am putut accesa pulsoximetrul</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="481"/>
        <source>Starting up...</source>
        <translation>POrnesc...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="482"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Daca puteti inca citi asta dupa cateva secunde, anulati si incercati din nou</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="529"/>
        <source>Live Import Stopped</source>
        <translation>Importul direct s-a oprit</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 sesiune(i) pe %2, pornite la %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="586"/>
        <source>No CPAP data available on %1</source>
        <translation>Nu exista date CPAP disponibile pe %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="594"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="721"/>
        <source>Recording...</source>
        <translation>Inregistrez...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="728"/>
        <source>Finger not detected</source>
        <translation>Degetul nu a fost detectat</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="828"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Vreau sa folosesc ora computerului pentru  aceasta sesiune de oximetrie.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="831"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Pulsoximetrul meu nu are ceas intern, trebuie sa setez manual ora.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="843"/>
        <source>Something went wrong getting session data</source>
        <translation>Nu am reusit sa obtin datele acestei sesiuni</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1132"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Bun venit la incarcarea semiautomata a oximetriei</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1134"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>PulsOximetrele sunt dispozitive medicale care masoara saturatia in oxigen a sangelui capilar. In timpul evenimentelor de apnee in somn si a respiratiei anormale, saturatia oxigenului din sange  poate scadea semnificativ necesitand consultul unui medic.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation>OSCAR este momentan compatibil cu pulsoximetrele Contec CMS50D+, CMS50E, CMS50F si  CMS50I seriale (USB).&lt;br/&gt;(Nota: Importul direct din modelele cu bluetooth &lt;span style=&quot; font-weight:600;&quot;&gt;probabil nu este posibil inca&lt;/span&gt;)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Poate vreti sa stiti, alte companii cum e Pulox, pur si simplu rebrand-uiesc pulsoximetrele Contec CMS50 sub alta denumire Pulox PO-200, PO-300, PO-400. Aceste aparate ar trebui sa fie compatibile cu OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>OSCAR poate deasemenea descifra fisierele DAT ale pulsoximetrului ChoiceMMed MD300W1.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>Please remember:</source>
        <translation>Amintiti-va:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>Important Notes:</source>
        <translation>Iimportant:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Dispozitivul Contec CMS50D+ nu are  un ceas intern si nu inregistreaza timpul. Daca nu aveti deja o sesiune CPAP descarcata cu care sa sincronizati inregistrarea din CMS50D+, va trebui sa introduceti manual ora de inceput dupa ce se finalizeaza importul datelor.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Chiar si la pulsoximetrele cu ceas intern, este recomandat sa incepeti sesiunea de oximetrie concomitent cu cea de CPAP, deoarece ceasul aparatelor CPAP are obiceiul sa ramana in urma cu timpul si nu toate pot fi resetate cu usurinta.</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy h:mm:ss AP</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;eset</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>&amp;Deschide .spo/R File</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>Serial &amp;Import</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>&amp;Start monitorizare in timp real</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Serial Port</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Rescaneaza porturile</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation>Preferinte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation>&amp;Importa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation>Combina sesiunile inchise </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="732"/>
        <source>Minutes</source>
        <translation>Minute</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Mai multe sesiuni mai strânse decât această valoare vor fi păstrate în aceeași zi.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation>Ignora sesiunile scurte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sesiunie cu durata mai scurta de atat nu vor fi afisate&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation>Timpul de impartire a zilei</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sesiunile care incep inainte de acest moment vor fi asociate zilei calendaristice precedente.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation>Setari stocare sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="440"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Comprima BAckup-urile SD (mai lent la primul import, dar face BAckup-urile mai mici)</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Changing SD Backup compression options doesn&apos;t automatically recompress backup data.  &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Schimbarea compresiei backup-ului SD nu reface compresia datelor deja existente &lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="638"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1268"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Considera zilele sub acest nivel ca necompliante. 4 ore de obicei e considerat complianta.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1271"/>
        <source> hours</source>
        <translation> ore</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="946"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation>Activați / dezactivați îmbunătățirile de înregistrare a evenimentelor experimentale.
Permite detectarea evenimentelor limită, iar unele mașini au ratat.
Această opțiune trebuie activată înainte de importare, altfel este necesară o curățare.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1024"/>
        <source>Flow Restriction</source>
        <translation>Restrictie a fluxului de aer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1065"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Procentul de restricție în fluxul de aer de la valoarea mediană.
O valoare de 20% funcționează bine pentru detectarea apnee. </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="988"/>
        <location filename="../oscar/preferencesdialog.ui" line="1069"/>
        <location filename="../oscar/preferencesdialog.ui" line="1546"/>
        <location filename="../oscar/preferencesdialog.ui" line="1706"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1042"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Selectarea manuala este o metoda experimentala de a tetecta manual evenimente ratate de catre aparat. Ele  &lt;span style=&quot; text-decoration: underline;&quot;&gt;nu sunt incluse&lt;/span&gt; in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1086"/>
        <source>Duration of airflow restriction</source>
        <translation>Durata restrictiei fluxului de aer</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="978"/>
        <location filename="../oscar/preferencesdialog.ui" line="1089"/>
        <location filename="../oscar/preferencesdialog.ui" line="1563"/>
        <location filename="../oscar/preferencesdialog.ui" line="1651"/>
        <location filename="../oscar/preferencesdialog.ui" line="1680"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1125"/>
        <source>Event Duration</source>
        <translation>Durata eveniment</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1112"/>
        <source>Allow duplicates near machine events.</source>
        <translation>Permiteti duplicate langa evenimentele detectate de aparat.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1190"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Reglează cantitatea de date luată în considerare pentru fiecare punct din graficul AHI / Ora.
Implicit la 60 de minute .. Vă recomandăm foarte mult să rămână la această valoare.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1194"/>
        <source> minutes</source>
        <translation> minute</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1233"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Reseteaza counterul la zero la inceputul fiecarei ferestre de timp.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1236"/>
        <source>Zero Reset</source>
        <translation>Zero Reset</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="685"/>
        <source>CPAP Clock Drift</source>
        <translation>Intarzierea ceasului CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="501"/>
        <source>Do not import sessions older than:</source>
        <translation>Nu importa sesiunile mai vechi de:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="508"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Sesiunile mai vechi decat aceasta data nu vor fi importate</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="534"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1249"/>
        <source>User definable threshold considered large leak</source>
        <translation>Utilizare prag configurabil pentru scurgere mare pe langa masca</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1252"/>
        <source> L/min</source>
        <translation> L/min</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1216"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Daca arată linia rosie în graficul scurgerilor din masca</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1793"/>
        <location filename="../oscar/preferencesdialog.ui" line="1865"/>
        <source>Search</source>
        <translation>Cautare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1489"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1079"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Afișați in diagrama grafică a evenimentelor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1102"/>
        <source>#1</source>
        <translation>#1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1008"/>
        <source>#2</source>
        <translation>#2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1001"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation>Resincronizeaza Evenimentele detectate de aparat (Experimental)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1641"/>
        <source>SPO2</source>
        <translation>SPO2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1703"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>Procent reducere in saturatia oxigenului</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1696"/>
        <source>Pulse</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1661"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Schimbari bruste n Puls cel putin in acest grad</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1553"/>
        <location filename="../oscar/preferencesdialog.ui" line="1583"/>
        <location filename="../oscar/preferencesdialog.ui" line="1664"/>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1648"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Scaderea oxigenului cu Durata minima</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1677"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Eveniment de schimbare a pulsului cu durata minima.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1560"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Mici fragmente de date oximetrice sub acest nivel se vor pierde.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1921"/>
        <source>&amp;General</source>
        <translation>&amp;General</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1316"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Schimbarile urmatoarelor setari necesita repornirea programului dar nu si recalcularea.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1319"/>
        <source>Preferred Calculation Methods</source>
        <translation>Metode de calcul preferate</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1348"/>
        <source>Middle Calculations</source>
        <translation>Calcule de mijloc</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1362"/>
        <source>Upper Percentile</source>
        <translation>Percentila superioara</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation>Setari impartire sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Aceste setari trebuie utilizate cu grija...&lt;/span&gt;Dezactivarea are consecințe care implică acuratețea rezumatului numai de câteva zile, deoarece anumite calcule funcționează doar în mod corespunzător doar daca  sesiunile rezumate care au provenit din înregistrările individuale sunt păstrate împreună. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Utilizatorii ResMed:&lt;/span&gt; Doar pentru că ți se pare natural ca tu și eu să reîncepi reluarea sesiunii de 12 luni ar trebui să fie în ziua precedentă, nu înseamnă că datele ResMed sunt de acord cu noi. Formatul indexului rezumat STF.edf prezintă slăbiciuni grave care fac ca aceasta să nu fie o idee bună. .&lt;/p&gt;&lt;p&gt;Această opțiune există pentru a pacifica pe cei care nu le pasă și doresc să vadă acest lucru &amp;quot;reparat&amp;quot; indiferent de costuri, dar știu că vine cu un cost. Dacă țineți cardul SD în fiecare noapte și importați cel puțin o dată pe săptămână, nu veți vedea probleme cu acest lucru foarte des.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Nu imparti sumarele zilelor (Atentie: cititi Tooltip-ul!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="568"/>
        <source>Memory and Startup Options</source>
        <translation>Optiuni memorie si pornire</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="610"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Incarca de la inceput toate rezumatele la lansare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="597"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Această setare păstrează datele și evenimente din memorie după utilizare pentru a accelera vizualizarea zilelor precedente.&lt;/p&gt;&lt;p&gt;Aceasta nu este cu adevărat o opțiune necesară, deoarece sistemul de operare cache conține și fișierele utilizate anterior..&lt;/p&gt;&lt;p&gt;Recomandarea este lăsarea acesteia oprită, cu excepția cazului în care computerul dvs are o tona de memorie..&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="600"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Mentine in memorie graficele</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="624"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Elimina confirmarile inutile in timpul Importului.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="627"/>
        <source>Import without asking for confirmation</source>
        <translation>Importa fara sa ceri confirmare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="782"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation>Acest calcul necesită date despre scurgerile totale de la aparatul CPAP. (De exemplu PRS1, dar nu si ResMed, care tine deja evidenta)

Calculele de scurgeri neintenționate utilizate aici sunt liniare, nu influienteaza curba de ventilare a măștii.

Dacă utilizați câteva măști diferite, alegeți în schimb valorile medii. Ar trebui să fie destul de aproape.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="789"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Calculeaza scurgerile neintentionate cand nu sunt prezente</translation>
    </message>
    <message>
        <source>Your masks vent rate at 20cmH2O pressure</source>
        <translation type="vanished">Rata de aerisire la presiunea de 20cmH2O</translation>
    </message>
    <message>
        <source>Your masks vent rate at 4cmH2O pressure</source>
        <translation type="vanished">Rata de aerisire la presiunea de 4cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="927"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Notă: Este utilizată o metodă de calcul liniar. Modificarea acestor valori necesită o recalculare.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1154"/>
        <source>General CPAP and Related Settings</source>
        <translation>Setari Generale CPAP si inrudite</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1163"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Activeaza graficul de evenimente necunoscute (UA)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1294"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1299"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translatorcomment>Indice de afectare respiratorie</translatorcomment>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1170"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>Graficul AHI/ora in Timp</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1226"/>
        <source>Preferred major event index</source>
        <translatorcomment>Index de evenimente majore preferat</translatorcomment>
        <translation>Indexul de evenimente majore preferat</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1177"/>
        <source>Compliance defined as</source>
        <translation>Complianta definita ca</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1219"/>
        <source>Flag leaks over threshold</source>
        <translation>Atentionare scurgeri peste limita admisa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="766"/>
        <source>Seconds</source>
        <translation>Secunde</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="712"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Acest lucru nu este destinat corecțiilor de fus orar! Asigurați-vă că ceasul și fusul orar al sistemului de operare sunt setate corect.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="759"/>
        <source>Hours</source>
        <translation>Ore</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1325"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Pentru consistență, utilizatorii ResMed ar trebui să folosească 95% aici,
deoarece aceasta este singura valoare disponibilă în zilele rezumate.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1376"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Median e recomandat pentru utilizatorii de ResMed.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1380"/>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>Median</source>
        <translation>Median</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1385"/>
        <source>Weighted Average</source>
        <translation>Media ponderata</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1390"/>
        <source>Normal Average</source>
        <translation>Medie normala</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1414"/>
        <source>True Maximum</source>
        <translation>ADEVARATUL mAXIM</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1419"/>
        <source>99% Percentile</source>
        <translation>Percentila 99%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1355"/>
        <source>Maximum Calcs</source>
        <translation>Calcule MInime</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1950"/>
        <source>General Settings</source>
        <translation>Setari Generale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2677"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Butoanele de navigare Vedere zilnica sar peste zile fara inregistrarile de date</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2680"/>
        <source>Skip over Empty Days</source>
        <translation>Sari peste zilele fara inregistrari</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1971"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Permiteți utilizarea mai multor nuclee de CPU acolo unde acestea sunt disponibile pentru a îmbunătăți performanța.
Afectează în principal importatorul.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1975"/>
        <source>Enable Multithreading</source>
        <translation>Activeaza Multithreading</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="574"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Sariti peste ecranul de conectare si incarcati cel mai recent utilizat Profil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="483"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Creaza un singur backup SD in timpul Importului (daca dezactivati asta o patiti!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1410"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adevartul maxim este maximul setului de date.&lt;/p&gt;&lt;p&gt;Percentila 99th filtreaza valorile discrepante.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1428"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Numărătoare combinată împărțită la Total ore</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1433"/>
        <source>Time Weighted average of Indice</source>
        <translation>Media ponderată  a indicelui</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1438"/>
        <source>Standard average of indice</source>
        <translation>Media standard a indiccilor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1369"/>
        <source>Culminative Indices</source>
        <translation>Indici de varf</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="951"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Evenimente CPAP semnalate de utilizator</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1770"/>
        <source>Events</source>
        <translation>Evenimente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1816"/>
        <location filename="../oscar/preferencesdialog.ui" line="1895"/>
        <source>Reset &amp;Defaults</source>
        <translation>Resetare la &amp;valorile initiale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1829"/>
        <location filename="../oscar/preferencesdialog.ui" line="1908"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Atentie: &lt;/span&gt;Doar pentru ca puteti, nu inseamna si ca e o idee buna.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1842"/>
        <source>Waveforms</source>
        <translation>Variatii grafice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1629"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Atentionare variatii rapide in nivelul oximetriei</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1540"/>
        <source>Other oximetry options</source>
        <translation>Alte setari Oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1590"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation>Atentionare saturatie sub limita admisa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1573"/>
        <source>Discard segments under</source>
        <translation>Elimina segmentele sub</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1610"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Atentionare Puls peste limita admisa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1600"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Atentionare Puls sub limita admisa</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="432"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation>Comprimați copilei de rezervă ResMed (EDF) pentru a economisi spațiu pe disc.
Fișierele EDF blocate sunt stocate în format .gz,
care este comun pe platformele Mac &amp; Linux ..

OSCAR poate importa nativ din acest director backup comprimat  ..
Pentru a-l utiliza cu ResScan va fi necesar ca fișierele .gz să fie intai decomprimate.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="452"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation>Următoarele opțiuni afectează cantitatea de spațiu pe disc pe care OSCAR o utilizează și au un efect asupra duratei importului.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="462"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation>Acest lucru face ca datele OSCAR să dureze aproximativ jumătate de spațiu.
Dar face ca importul și schimbarea zilei să dureze mai mult ..
Dacă aveți un computer nou cuspatiu putin, aceasta este o opțiune bună.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="467"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation>Comprima Datele Sesiunii (face datele OSCAR mai mici, dar schimbarea zilei mai lenta)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="474"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation>Aceasta menține o copie de rezervă a datelor de pe cardul SD pentru mașinile ResMed,

Aparatele ResMed S9 pot șterge date de înaltă rezoluție mai vechi de 7 zile,
și date grafice mai vechi de 30 de zile ..

OSCAR poate păstra o copie a acestor date dacă vreți să reinstalați.
(Foarte recomandat, cu excepția cazului în scurt timp pe spațiu pe disc sau nu-i pasă de datele din grafic)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Înainte de încărcarea tuturor datelor de sinteză, OSCAR începe un pic mai lent, ceea ce accelerează răsfoirea generală și câteva alte calcule ulterioare. &lt;/ p&gt; &lt;p&gt; Dacă aveți o cantitate mare de date, s-ar putea să fie păstrată această oprire, dar dacă doriți să vedeți &lt;span style=&quot; font-style:italic;&quot;&gt;totul&lt;/span&gt; in Vedere de ansamblu, toate datele de sinteză trebuie încărcate oricum. &lt;/p&gt;&lt;p&gt;Rețineți că această setare nu afectează datele de undă și de evenimente, care sunt întotdeauna solicitate a fi  încărcate după cum este necesar.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="885"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="895"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="998"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation>Această opțiune experimentală încearcă să utilizeze sistemul de semnalizare a evenimentelor OSCAR pentru a îmbunătăți poziționarea evenimentului detectat de aparat.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1160"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation>Afișați steaguri pentru evenimente detectate de mașină care nu au fost încă identificate.</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Sincronizare date Oximetrie si CPAP&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data importata din SpO2Review (dintr-un fisier .spoR) sau prin cablul serial &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;nu are asociata ora corecta&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; necesara pentru sincronizare.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Modul direct in timp real (folosind un cablu serial) este o modalitate de a obtine o sincronizare exacta cu pulsoximetrul CMS50, dar nu contracareaza deviatia ceasului din CPAP.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Daca porniti pulsoximetrul &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;in acelasi moment in care porniti CPAP, obtineti deasemenea sincronizarea inregistrarilor CPAP si oximetrie . &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Descarcarea oximetriei prin cablul serial preia ora de inceput a sesiunii CPAP din noaptea precedenta. (Amintiti-va sa descarcati intai datele din CPAP si apoi pe cele din pulxosimetru!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1982"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation>Afișați notificarea Reminder Scoateti Cardul la oprirea OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2014"/>
        <source>Automatically Check For Updates</source>
        <translation>Verifica automat daca ezista actualizari</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2031"/>
        <source>Check for new version every</source>
        <translation>Verifica daca exista o noua versiune la fiecare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2038"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation>SourceForge gazduieste acest proiect gratuit.. Va rog donati si la ei pe site..</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2054"/>
        <source>days.</source>
        <translation>zile.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2119"/>
        <source>&amp;Check for Updates now</source>
        <translation>&amp;Verifica acum daca exista noutati</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2142"/>
        <source>Last Checked For Updates: </source>
        <translation>Ultia cautare de actualizari: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2155"/>
        <source>TextLabel</source>
        <translation>Eticheta Text</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2177"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Daca doriti sa ajutati testand mai devreme noi facilitati ale programului OSCAR, click aici.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Dar va rog sa intelegeti ca asta poate insemna uneori o functionare defectuoasa a programului...&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2186"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation>Vreau să încerc variante  experimentale și de testare ale OSCAR (numai utilizatorii avansați vă rog.)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2210"/>
        <source>&amp;Appearance</source>
        <translation>&amp;Aspect</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2239"/>
        <source>Graph Settings</source>
        <translation>Setari Grafic</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2255"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Ce filă se deschide la încărcarea unui profil. (Notă: implicit va fi pagina de Profiel dacă OSCAR este setat să nu deschidă un profil la pornire)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2513"/>
        <source>Bar Tops</source>
        <translation>Barele de sus</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2518"/>
        <source>Line Chart</source>
        <translation>Linia Graficului</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2608"/>
        <source>Overview Linecharts</source>
        <translation>Vedere de ansamblu Grafice liniare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2729"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation>Încercați să modificați această setare din setarea implicită (Desktop OpenGL) dacă întâmpinați probleme de redare cu graficele OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2553"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aceasta face derularea  mai ușoara pe TouchPad-urile bidirecționale sensibile atunci când imaginea este mărită&lt;/p&gt;&lt;p&gt;50ms este valoarea recomandata.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2447"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Cat vreti sa ramana vizibile Tootips.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2435"/>
        <source>Scroll Dampening</source>
        <translation>Derulare atenuare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2425"/>
        <source>Tooltip Timeout</source>
        <translation>Tooltip Timeout</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2532"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Înălțimea de afișare implicită a graficelor în pixeli</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2505"/>
        <source>Graph Tooltips</source>
        <translation>Tooltips pe Grafic</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2381"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>Metoda vizuala de afisare acundelor e suprapune peste atentionari.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2386"/>
        <source>Standard Bars</source>
        <translation>Bare de unelte standard</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2391"/>
        <source>Top Markers</source>
        <translation>Producatori de top</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2339"/>
        <source>Graph Height</source>
        <translation>Inaltime Grafic</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="422"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="577"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Lanseaza automat Importatorul de CPAP dupa incarcarea Profilului</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="617"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Incarca automat la pornire ultimul Profil utilizat</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="798"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="854"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1457"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Nota: &lt;/span&gt;Din cauza limitarilor rezumatului,aparatele  ResMed nu permit schimbarea acestor setari.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1510"/>
        <source>Oximetry Settings</source>
        <translation>Setari Oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1750"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2245"/>
        <source>On Opening</source>
        <translation>La Deschidere</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2258"/>
        <location filename="../oscar/preferencesdialog.ui" line="2262"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2267"/>
        <location filename="../oscar/preferencesdialog.ui" line="2306"/>
        <source>Welcome</source>
        <translation>Bun venit</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2272"/>
        <location filename="../oscar/preferencesdialog.ui" line="2311"/>
        <source>Daily</source>
        <translation>Zilnic</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2282"/>
        <location filename="../oscar/preferencesdialog.ui" line="2321"/>
        <source>Statistics</source>
        <translation>Statistici</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2290"/>
        <source>Switch Tabs</source>
        <translation>Schimba ferestrele</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2301"/>
        <source>No change</source>
        <translation>Nicio schimbare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2329"/>
        <source>After Import</source>
        <translation>Dupa Import</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2355"/>
        <source>Overlay Flags</source>
        <translation>Atentionari suprapuse</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2365"/>
        <source>Line Thickness</source>
        <translation>Grosimea Liniei</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2401"/>
        <source>The pixel thickness of line plots</source>
        <translation>Grosimea pixelului din linii</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2627"/>
        <source>Other Visual Settings</source>
        <translation>Alte setari Vizuale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2633"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>Anti-Aliasing aplică netezirea graficelor.
Anumite parcele arata mai atragatoare cu asta.
Acest lucru afectează de asemenea rapoartele tipărite.

Încearcă și vezi dacă îți place.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2640"/>
        <source>Use Anti-Aliasing</source>
        <translation>Utilizare Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2647"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Face anumite plots sa arate mai ca &quot;undele patrate&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2650"/>
        <source>Square Wave Plots</source>
        <translation>Unde patrate</translation>
    </message>
    <message>
        <source>Allows graphs to be &quot;screenshotted&quot; for display purposes.
The Event Breakdown PIE chart uses this method, as does
the printing code.
Unfortunately some older computers/versions of Qt can cause
this application to be unstable with this feature enabled.</source>
        <translation type="vanished">Permite graficelor să fie &quot;screenshotted&quot; pentru afișare.
Grficul PIE Event Breakdown utilizează această metodă, la fel
codul de imprimare.
Din păcate, unele calculatoare mai vechi / versiuni ale Qt pot provoca
această aplicație să fie instabilă cu această funcție activată.</translation>
    </message>
    <message>
        <source>Show event breakdown pie chart</source>
        <translation type="vanished">Afișați diagrama grafică a evenimentelor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2657"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Pixmap caching este o tehnică de accelerare grafică. Poate provoca probleme cu desenarea fontului în zona de afișare a graficelor de pe platforma dvs.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2660"/>
        <source>Use Pixmap Caching</source>
        <translation>Utilizare Pixmap Caching</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2667"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Aceste caracteristici au fost recent eliminate. Elevor fi reintroduse  mai târziu. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2670"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animatii &amp;&amp; Fancy Stuff</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2687"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Daca să permități modificarea scalei  axei y prin dublu clic pe etichetele axei x</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2690"/>
        <source>Allow YAxis Scaling</source>
        <translation>Permite scalarea pe axa Y</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2697"/>
        <source>Whether to include machine serial number on machine settings changes report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2700"/>
        <source>Include Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2723"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Graphics Engine (necesita Restart)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2754"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Fonts (valabile peste tot in aplicatie)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2789"/>
        <source>Font</source>
        <translation>Font</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2808"/>
        <source>Size</source>
        <translation>Dimensiune</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2827"/>
        <source>Bold  </source>
        <translation>Bold  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2849"/>
        <source>Italic</source>
        <translation>Italic</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2862"/>
        <source>Application</source>
        <translation>Aplicatie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2926"/>
        <source>Graph Text</source>
        <translation>Textul Graficului</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2987"/>
        <source>Graph Titles</source>
        <translation>Titlul Graficului</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3048"/>
        <source>Big  Text</source>
        <translation>Text Mare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3115"/>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <location filename="../oscar/preferencesdialog.cpp" line="576"/>
        <source>Details</source>
        <translation>Detalii</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3159"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuleaza</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3166"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="439"/>
        <location filename="../oscar/preferencesdialog.cpp" line="570"/>
        <source>Name</source>
        <translation>Nume</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="440"/>
        <location filename="../oscar/preferencesdialog.cpp" line="571"/>
        <source>Color</source>
        <translation>Culoare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="442"/>
        <source>Flag Type</source>
        <translation>Tip Atentionare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="443"/>
        <location filename="../oscar/preferencesdialog.cpp" line="575"/>
        <source>Label</source>
        <translation>Eticheta</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="460"/>
        <source>CPAP Events</source>
        <translation>Evenimente CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="461"/>
        <source>Oximeter Events</source>
        <translation>Evenimente Oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="462"/>
        <source>Positional Events</source>
        <translation>Evenimente posturale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="463"/>
        <source>Sleep Stage Events</source>
        <translation>Evenimente stadiu de somn</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="464"/>
        <source>Unknown Events</source>
        <translation>Evenimente necunoscute</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="636"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>DubluClick pentru a schimbadescrierea acestui parametru.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="514"/>
        <location filename="../oscar/preferencesdialog.cpp" line="643"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>DubluClick pentru a schimba culoarea implicita pentru acest parametru plot/flag/data.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2277"/>
        <location filename="../oscar/preferencesdialog.ui" line="2316"/>
        <location filename="../oscar/preferencesdialog.cpp" line="441"/>
        <location filename="../oscar/preferencesdialog.cpp" line="572"/>
        <source>Overview</source>
        <translation>Vedere de ansamblu</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="65"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;b&gt;Nota:&lt;/b&gt;Capabilitatile avansate ale programului OSCAR de a analiza inregistrarile nu sunt posibile cu aparatele &lt;b&gt;ResMed&lt;/b&gt; datorită limitării modului în care sunt stocate setările și datele sumare și prin urmare, au fost dezactivate pentru acest profil.&lt;/p&gt;&lt;p&gt;Pe aparatele ResMed zilele vor fi  &lt;b&gt;despartite la pranz&lt;/b&gt;ca insoftul comercial al  ResMed.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="506"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>DubluClick pentru a schimba numele descrierii parametrului %1.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="519"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Indiferent dacă acest steag are o diagramă dedicată generală.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="529"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Aici puteți schimba tipul de steag prezentat pentru acest eveniment</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="534"/>
        <location filename="../oscar/preferencesdialog.cpp" line="667"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Aceasta este o eticheta scurta pentru a identifica pe ecran acest parametru.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="540"/>
        <location filename="../oscar/preferencesdialog.cpp" line="673"/>
        <source>This is a description of what this channel does.</source>
        <translation>Aceasta este o descriere a ceea ce face acest parametru.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="573"/>
        <source>Lower</source>
        <translation>Mai jos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="574"/>
        <source>Upper</source>
        <translation>Mai sus</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="593"/>
        <source>CPAP Waveforms</source>
        <translation>Unde CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="594"/>
        <source>Oximeter Waveforms</source>
        <translation>Pletismograma</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="595"/>
        <source>Positional Waveforms</source>
        <translation>Unde posturale</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="596"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Unde  stadiu de somn</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="652"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Dacă o defalcare a acestei forme de undă este afișată în prezentarea generală.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="657"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aici puteți schimba pragul &lt;b&gt;minim&lt;/b&gt; utilizat pentru anumite calcule ale undei %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="662"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aici puteți schimba pragul &lt;b&gt;maxim&lt;/b&gt; utilizat pentru anumite calcule ale undei %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="772"/>
        <source>Data Processing Required</source>
        <translation>E nevoie de procesarea datelor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="773"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Este necesară o procedură de re / decompresie a datelor pentru a aplica aceste modificări. Această operație poate dura câteva minute până la finalizare.

Sigur doriți să faceți aceste modificări?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="781"/>
        <source>Data Reindex Required</source>
        <translation>E nevoie de reindexarea datelor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="782"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Este necesară o procedură de reindexare a datelor pentru a aplica aceste modificări. Această operație poate dura câteva minute până la finalizare.

Sigur doriți să faceți aceste modificări?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="788"/>
        <source>Restart Required</source>
        <translation>Este necesara repornirea</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="789"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Una sau mai multe dintre modificările pe care le-ați făcut vor necesita reînceperea acestei aplicații, pentru ca aceste modificări să intre în vigoare.

Vreti să faceti asta acum?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1145"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation> Dacă vreți să reimportați din nou aceste date (fie în OSCAR sau ResScan), aceste date nu vor maiputea fi recuperate.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1146"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation> Dacă aveți nevoie pentru a economisi spațiu pe disc, vă rugăm să rețineți că efectuați backup-uri manuale.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1147"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation> Sigur doriti sa dezactivati aceste Backup-uri?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1191"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation>Dezactivarea Backup-urior nu e o ide buna, deoarece OSCAR are nevoie de acestea pentru a reconstrui baza de date in caz ca apar erori.

</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1192"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Sigur doriti sa faceti asta?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="299"/>
        <location filename="../oscar/preferencesdialog.cpp" line="300"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1263"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1268"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Flag</source>
        <translation>Atentionare</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Minor Flag</source>
        <translation>Atentionare minora</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Span</source>
        <translation>Span</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Always Minor</source>
        <translation>Intotdeauna Minor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="272"/>
        <source>Never</source>
        <translation>Niciodata</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1143"/>
        <source>This may not be a good idea</source>
        <translation>S-ar putea sa nu fi e o idee asa buna</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1144"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation>Aparatele ResMed S9 sterg de obicei anumite date mai vechi de 7 si 30 zile din cardul SD (in functie de rezolutie).</translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation>Alegeti Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation>Cautare:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation>Incepe cu utilizatorul de Profil selectat.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation>&amp;Alegeti utilizator</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation>Creati un nou utilizator de Profil.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation>Profil nou</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation>Alegeti un alt dosar de date pentru OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation>Click aici daca nu ati dorit sa porniti OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation>Locatia curenta a bazei de date OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation>Alt &amp;Dosar</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation>[version]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation>&amp;Inchideti aplicatia</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation>Dosar:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translation>[data directory]</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation>Deschide Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation>Editeaza profil OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation>Stergeti un Profil OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation>Introduceti Parola pentru %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation>Parola incorecta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation>Ati introdus o parola gresita de prea multe ori.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translation>Introduceti cuvantul DELETE dedesubt pentru a confirma stergerea.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation>Sunteti pe cale sa eliminati Profilul &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation>Imi pare rau</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Trebuie sa introduceti cuvantul DELELE cu litere MARI.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation>Ati introdus o parola gresita</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation>Daca incercati stergerea pentru ca ati uitat parola, trebuie sa-l stergeti manual.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>A aparut o eroare la stergerea dosarului Profilului, o sa trebuiasca sa-l stergeti manual.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profilul &apos;%1&apos; a fost sters</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation>Creati un nou Profil OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation>Introduceti Parola</translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation>Ati introdus o parola gresita de prea multe ori. Aici ne oprim!</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filtru:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="55"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="184"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="199"/>
        <source>Version</source>
        <translation>Versiunea</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="216"/>
        <source>&amp;Open Profile</source>
        <translation>&amp;Deschide Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="227"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editeaza Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="241"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Profil nou</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="260"/>
        <source>Profile: None</source>
        <translation>Profil: niciunul</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="281"/>
        <source>Please select or create a profile...</source>
        <translation>Va rog selectati sau creat un profil...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="332"/>
        <source>Destroy Profile</source>
        <translation>Elimina Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="89"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Ventilator Brand</source>
        <translation>Brandul aparatului</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Model</source>
        <translation>Modelul CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Other Data</source>
        <translation>Alte Date</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Last Imported</source>
        <translation>Ultimul importat</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Name</source>
        <translation>Nume</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="134"/>
        <location filename="../oscar/profileselector.cpp" line="311"/>
        <source>%1, %2</source>
        <translation>%1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="169"/>
        <source>You must create a profile</source>
        <translation>Trebuie sa creati un Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="232"/>
        <location filename="../oscar/profileselector.cpp" line="362"/>
        <source>Enter Password for %1</source>
        <translation>Introduceti parola pentru %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="248"/>
        <location filename="../oscar/profileselector.cpp" line="381"/>
        <source>You entered an incorrect password</source>
        <translation>Ati introdus o parola gresita</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="251"/>
        <source>Forgot your password?</source>
        <translation>Ati uitat parola?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="251"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Intrebati p eforum cum sa faceti reset, e destul de simplu.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="321"/>
        <source>Select a profile first</source>
        <translation>Selectati intai un Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="384"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Daca incercati sa stergeti profilul pentru ca ati uitat parola, mai bine o resetati, sau daca nu, trebuie sa stergeti manual dosarul.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="394"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Sunteti pe cale sa eliminati Profilul &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="394"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Ganditi-va bine, asta va sterge iremediabil Profilul impreuna cu toate&lt;b&gt;backup-urile&lt;/b&gt; salvate in el&lt;br/&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="394"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Introducet cuvantul&lt;b&gt;DELETE&lt;/b&gt; mai jos (exact asa cum se vede) pentru a confirma.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="412"/>
        <source>DELETE</source>
        <translation>DELETE</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="413"/>
        <source>Sorry</source>
        <translation>Imi pare rau</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="413"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Introducet cuvantul&lt;b&gt;DELETE&lt;/b&gt; cu litere MARI.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="426"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>A aparut o eroare la stergerea Profilului, trebuie sa stergeti manual dosarul acestuia.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="430"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Profilul &apos;%1&apos; a fost creat</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="460"/>
        <source>Summaries:</source>
        <translation>Rezumate:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="461"/>
        <source>Events:</source>
        <translation>Evenimente:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="462"/>
        <source>Backups:</source>
        <translation>Backup-uri:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="474"/>
        <location filename="../oscar/profileselector.cpp" line="514"/>
        <source>Hide disk usage information</source>
        <translation>Ascunde utilizare disk</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="477"/>
        <source>Show disk usage information</source>
        <translation>Arata utilizare disk</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="495"/>
        <source>Name: %1, %2</source>
        <translation>Nume: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="498"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="501"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="504"/>
        <source>Address:</source>
        <translation>Adresa:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="507"/>
        <source>No profile information given</source>
        <translation>Nu au fost furnizate informatii de Profil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="510"/>
        <source>Profile: %1</source>
        <translation>Profil: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation>Anuleaza</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation>Lipsa Date</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="366"/>
        <source>Events</source>
        <translation>Evenimente</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="364"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <source>Duration</source>
        <translation>Durata</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="381"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 in evenimente)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jan</source>
        <translation>Ian</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Feb</source>
        <translation>Feb</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Mar</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Apr</source>
        <translation>Apr</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>May</source>
        <translation>Mai</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jun</source>
        <translation>Iun</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jul</source>
        <translation>Iul</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Aug</source>
        <translation>Aug</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Sep</source>
        <translation>Sep</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Oct</source>
        <translation>Oct</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Dec</source>
        <translation>Dec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>&quot;</source>
        <translation>&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Kg</source>
        <translation>Kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="198"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="216"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="247"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="257"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="252"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="262"/>
        <source>Max: </source>
        <translation>Max: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="266"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="270"/>
        <source>%1: </source>
        <translation>%1: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>???: </source>
        <translation>???: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="281"/>
        <source>Max: %1</source>
        <translation>Max: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="287"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 zile): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="289"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 zi): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="347"/>
        <source>% in %1</source>
        <translation>% in %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="353"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="680"/>
        <location filename="../oscar/SleepLib/common.cpp" line="668"/>
        <source>Hours</source>
        <translation>Ore</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="665"/>
        <source>
Hours: %1</source>
        <translation>
Ore: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="731"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 utilizare redusa, %2 neutilizat, din %3 zile (%4% compliant.) Lungime: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="812"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sesiunile: %1 / %2 / %3 Lungime: %4 / %5 / %6 Cea mai lunga: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="931"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Lungime: %3
Start: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="933"/>
        <source>Mask On</source>
        <translation>Masca conectata</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="933"/>
        <source>Mask Off</source>
        <translation>Masca deconectata</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="944"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Lungime: %3
Start: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1115"/>
        <source>TTIA:</source>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1128"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1229"/>
        <source>%1 %2 / %3 / %4</source>
        <translation>%1 %2 / %3 / %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Minutes</source>
        <translation>Minute</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <source>Seconds</source>
        <translation>Secunde</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="675"/>
        <source>Events/hr</source>
        <translation>Evenimente/ora</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>Litres</source>
        <translation>Litri</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <source>Breaths/min</source>
        <translation>Respiratii/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>Severity (0-1)</source>
        <translation>Severitate (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>Degrees</source>
        <translation>Grade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>Error</source>
        <translation>Eroare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>Warning</source>
        <translation>Avertisment</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>Information</source>
        <translation>Informatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>Busy</source>
        <translation>Ocupat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>Please Note</source>
        <translation>Va rog aveti grija</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Compliance Only :(</source>
        <translation>Doar Dte despre Complianta :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>Graphs Switched Off</source>
        <translation>Grafic dezactivat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Summary Only :(</source>
        <translation>Doar Sumar :(</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>Sessions Switched Off</source>
        <translation>Sesiunile au fost oprite</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>&amp;Yes</source>
        <translation>&amp;Da</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>&amp;No</source>
        <translation>&amp;Nu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="705"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuleaza</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="706"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Elimina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>&amp;Save</source>
        <translation>&amp;Salvare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>BMI</source>
        <translation>IMC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>Weight</source>
        <translation>Greutate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Zombie</source>
        <translation>Zombie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="712"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Pulse Rate</source>
        <translation>Puls</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2</source>
        <translation>SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethy</source>
        <translation>Plethy</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <source>Pressure</source>
        <translation>Presiune</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Daily</source>
        <translation>Zilnic</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <source>Overview</source>
        <translation>Vedere de ansamblu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>Oximetry</source>
        <translation>Pulsoximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Oximeter</source>
        <translation>Pulsoximetru</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="723"/>
        <source>Event Flags</source>
        <translation>Avertismente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="726"/>
        <source>Default</source>
        <translation>Implicite</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3020"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="733"/>
        <source>Min EPAP</source>
        <translation>Min EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <source>Max EPAP</source>
        <translation>Max EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <source>Min IPAP</source>
        <translation>Min IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <source>Max IPAP</source>
        <translation>Max IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3021"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3027"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7966"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>Humidifier</source>
        <translation>Umidificator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="749"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="750"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SA</source>
        <translation>SA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="753"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="757"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7950"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <source>NRI</source>
        <translatorcomment>do not know what means</translatorcomment>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>AHI</source>
        <translation>AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>RDI</source>
        <translation>RDI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <source>AI</source>
        <translation>AI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <source>HI</source>
        <translation>HI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <source>UAI</source>
        <translation>UAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <source>CAI</source>
        <translation>CAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <source>FLI</source>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <source>REI</source>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <source>ÇSR</source>
        <translation>ÇSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>PB</source>
        <translation>PB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>IE</source>
        <translatorcomment>don`t know..</translatorcomment>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Insp. Time</source>
        <translation>Timp Insp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Exp. Time</source>
        <translation>Timp Expir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Resp. Event</source>
        <translation>Ev. Resp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limitation</source>
        <translation>Limitare Flux</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>Flow Limit</source>
        <translation>Limita Flux</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake</source>
        <translation>SensAwake</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <source>Pat. Trig. Breath</source>
        <translation>Respiratie Decl.de  Pacient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <source>Tgt. Min. Vent</source>
        <translation>Tinta Min. Vent</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Vent.</source>
        <translation>Tinta Vent.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="794"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Vent.</source>
        <translation>Vent.Mica.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Tidal Volume</source>
        <translation>Vol.Respirator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Resp. Rate</source>
        <translation>Frecv. Resp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Snore</source>
        <translation>Sforait</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>Leak</source>
        <translation>Scurgeri</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>Leaks</source>
        <translation>Scurgeri</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Large Leak</source>
        <translation>Scurgere semnificativa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>LL</source>
        <translation>Scapari importante</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leaks</source>
        <translation>Scurgeri totale</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <source>Unintentional Leaks</source>
        <translation>Scapari Neintentionale</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>MaskPressure</source>
        <translation>PresiuneMasca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Flow Rate</source>
        <translation>Flux</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Sleep Stage</source>
        <translation>Stadiul somnului</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <source>Usage</source>
        <translation>Utilizare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="808"/>
        <source>Sessions</source>
        <translation>Sesiuni</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="809"/>
        <source>Pr. Relief</source>
        <translation>Pr. Relief</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>OSCAR</source>
        <translation>OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>No Data Available</source>
        <translation>Nu sunt Date disponibile</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="239"/>
        <source>Built with Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="242"/>
        <source>Branch:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="244"/>
        <source>Revision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="246"/>
        <source>App key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="248"/>
        <source>Operating system:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="249"/>
        <source>Graphics Engine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="250"/>
        <source>Graphics Engine type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Software Engine</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="657"/>
        <source>ANGLE / OpenGLES</source>
        <translatorcomment>i have no idea..</translatorcomment>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <source>Desktop OpenGL</source>
        <translation>Desktop OpenGL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="660"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>Bookmarks</source>
        <translation>Semne de carte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <source>v%1</source>
        <translation>v%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3015"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3017"/>
        <source>Mode</source>
        <translation>Mod</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="816"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>Brand</source>
        <translation>Brand</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Serial</source>
        <translation>Serial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Series</source>
        <translation>Serie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <source>Machine</source>
        <translation>Aparat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <source>Channel</source>
        <translation>Parametru</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Settings</source>
        <translation>Setari</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Inclination</source>
        <translation>Inclinatie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Orientation</source>
        <translation>Orientare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <source>Name</source>
        <translation>Nume</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <source>DOB</source>
        <translatorcomment>I don`t know what it is.</translatorcomment>
        <translation>DOB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <source>Patient ID</source>
        <translation>ID pacient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Bedtime</source>
        <translation>Ora de culcare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Wake-up</source>
        <translation>Trezire</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>Mask Time</source>
        <translation>Timp cu Masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation>Necunoscut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <source>None</source>
        <translation>Niciuna</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>Ready</source>
        <translation>Pregatit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <source>First</source>
        <translation>Primul</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="843"/>
        <source>Last</source>
        <translation>Ultimul</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>Start</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="845"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>End</source>
        <translation>Sfarsit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="846"/>
        <source>On</source>
        <translation>Pornit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="847"/>
        <source>Off</source>
        <translation>Oprit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="848"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7997"/>
        <source>Yes</source>
        <translation>Da</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7998"/>
        <source>No</source>
        <translation>Nu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <source>Min</source>
        <translation>Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <source>Max</source>
        <translation>Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <source>Med</source>
        <translation>Med</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <source>Average</source>
        <translation>Medie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="856"/>
        <source>Median</source>
        <translation>Medie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="202"/>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <source>Avg</source>
        <translation>Med</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="200"/>
        <location filename="../oscar/SleepLib/common.cpp" line="858"/>
        <source>W-Avg</source>
        <translation>W-Mediu</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="772"/>
        <source>Non Data Capable Machine</source>
        <translation>Acest aparat CPAP nu suporta inregistrarea datelor</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="773"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation>Din pacate aparatul dvs  Philips Respironics CPAP (Model %1) nu este capabil sa inregistreze date.</translation>
    </message>
    <message>
        <source>RemStar Plus Compliance Only</source>
        <translation type="vanished">RemStar Plus doar Complianta</translation>
    </message>
    <message>
        <source>RemStar Pro with C-Flex+</source>
        <translation type="vanished">RemStar Pro cu C-Flex+</translation>
    </message>
    <message>
        <source>RemStar Auto with A-Flex</source>
        <translation type="vanished">RemStar Auto cu A-Flex</translation>
    </message>
    <message>
        <source>RemStar BiPAP Pro with Bi-Flex</source>
        <translation type="vanished">RemStar BiPAP Pro cu Bi-Flex</translation>
    </message>
    <message>
        <source>RemStar BiPAP Auto with Bi-Flex</source>
        <translation type="vanished">RemStar BiPAP Auto cu Bi-Flex</translation>
    </message>
    <message>
        <source>BiPAP autoSV Advanced</source>
        <translation type="vanished">BiPAP autoSV Advanced</translation>
    </message>
    <message>
        <source>BiPAP AVAPS</source>
        <translation type="vanished">BiPAP AVAPS</translation>
    </message>
    <message>
        <source>Unknown Model</source>
        <translation type="vanished">Model Necunoscut</translation>
    </message>
    <message>
        <source>System One (60 Series)</source>
        <translation type="vanished">System One (60 Series)</translation>
    </message>
    <message>
        <source>DreamStation</source>
        <translation type="vanished">DreamStation</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation type="vanished">necunoscut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="670"/>
        <source>Getting Ready...</source>
        <translation>Pregatesc...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="785"/>
        <source>Machine Unsupported</source>
        <translation>Aparatul nu poate fi folosit cu OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="786"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation>Imi pare rau, aparatul dvs  Philips Respironics CPAP  (Model %1) nu este compatibil momentan cu OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="774"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation>Imi pare rau, OSCAR poate urmari doar timpul de utilizare si cateva setari de baza pentru acest aparat CPAP.</translation>
    </message>
    <message>
        <source>The developers needs a .zip copy of this machines&apos; SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation type="vanished">Programatorii au nevoie de o copie zip a cardului SD al aparatului dvs CPAP si raportul Encore.pdf pentru a-l face sa functioneze cu OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="691"/>
        <source>Scanning Files...</source>
        <translation>Scanez fisierele...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="700"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1996"/>
        <source>Importing Sessions...</source>
        <translation>Importez Sesiunile...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="705"/>
        <source>Finishing up...</source>
        <translation>Finalizare....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="787"/>
        <source>The developers needs a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="806"/>
        <source>Machine Untested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="807"/>
        <source>Your Philips Respironics CPAP machine (Model %1) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="808"/>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="4070"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="6875"/>
        <source>%1mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8042"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8041"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <source>RemStar Plus</source>
        <translation type="vanished">RemStar Plus</translation>
    </message>
    <message>
        <source>BiPAP autoSV Advanced 60 Series</source>
        <translation type="vanished">BiPAP autoSV Advanced 60 Series</translation>
    </message>
    <message>
        <source>CPAP Pro</source>
        <translation type="vanished">CPAP Pro</translation>
    </message>
    <message>
        <source>Auto CPAP</source>
        <translation type="vanished">Auto CPAP</translation>
    </message>
    <message>
        <source>BiPAP Pro</source>
        <translation type="vanished">BiPAP Pro</translation>
    </message>
    <message>
        <source>Auto BiPAP</source>
        <translation type="vanished">Auto BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7954"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7956"/>
        <source>Flex Mode</source>
        <translation>Flex Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7955"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>Mod eliberare presiune PRS1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7961"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7962"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7963"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7964"/>
        <source>Rise Time</source>
        <translation>Tmp de crestere</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7965"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7970"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7972"/>
        <source>Flex Level</source>
        <translation>Flex Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7971"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>Setari presiune eliberare.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7976"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8007"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8029"/>
        <source>x1</source>
        <translation>x1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7977"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8008"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8030"/>
        <source>x2</source>
        <translation>x2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7978"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8009"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8031"/>
        <source>x3</source>
        <translation>x3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7979"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8010"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8032"/>
        <source>x4</source>
        <translation>x4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8011"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8033"/>
        <source>x5</source>
        <translation>x5</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7984"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7986"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3072"/>
        <source>Humidifier Status</source>
        <translation>Stare Umidificator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7985"/>
        <source>PRS1 humidifier connected?</source>
        <translation>Umidificatorul PRS1 e conectat?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7988"/>
        <source>Disconnected</source>
        <translation>Deconectat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7989"/>
        <source>Connected</source>
        <translation>Conectat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7993"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7995"/>
        <source>Heated Tubing</source>
        <translation>Tub incalzit</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7994"/>
        <source>Heated Tubing Connected</source>
        <translation>Tubul incalzit conectat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8002"/>
        <source>Humidification Level</source>
        <translation>Nivelul Umidificarii</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8003"/>
        <source>PRS1 Humidification level</source>
        <translation>Nivel Umidificare PRS1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8004"/>
        <source>Humid. Lvl.</source>
        <translation>Niv. Umid.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8015"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8016"/>
        <source>System One Resistance Status</source>
        <translation>Status Resistenta System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8017"/>
        <source>Sys1 Resist. Status</source>
        <translation>Sys1 Status Resist</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8024"/>
        <source>System One Resistance Setting</source>
        <translation>Setare Resistenta System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8025"/>
        <source>System One Mask Resistance Setting</source>
        <translation>Setari rezistenta System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8026"/>
        <source>Sys1 Resist. Set</source>
        <translation>Sys1 Setare Resist</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8037"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8039"/>
        <source>Hose Diameter</source>
        <translation>Diametrul tubului</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8038"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Diametru principalului furtun CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8046"/>
        <source>System One Resistance Lock</source>
        <translation>Blocare Resistenta System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8047"/>
        <source>Whether System One resistance settings are available to you.</source>
        <translation>Daca setarile rezistentei in aparatul dvs System One va sunt accesibile.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8048"/>
        <source>Sys1 Resist. Lock</source>
        <translation>Sys1 Blocare Resist</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8055"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8057"/>
        <source>Auto On</source>
        <translation>Auto activat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8056"/>
        <source>A few breaths automatically starts machine</source>
        <translation>Aparatul va porni automat dupa ce detecteaza cateva respiratii</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8064"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8066"/>
        <source>Auto Off</source>
        <translation>Auto dezactivat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8065"/>
        <source>Machine automatically switches off</source>
        <translation>Aparatul se opreste automat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8073"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8075"/>
        <source>Mask Alert</source>
        <translation>Alerta Masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8074"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation>Daca aparatul dvs permite verificarea Mastii.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8082"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8084"/>
        <source>Show AHI</source>
        <translation>Arata AHI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8083"/>
        <source>Whether or not machine shows AHI via LCD panel.</source>
        <translation>Daca aparatul dvs va arata AHI pe ecran.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8096"/>
        <source>Unknown PRS1 Code %1</source>
        <translation>Cod %1 PRS1 Necunoscut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8097"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8098"/>
        <source>PRS1_%1</source>
        <translation>PRS1_%1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8110"/>
        <source>Breathing Not Detected</source>
        <translation>Respiratia Nu a fost Detectata</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8111"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation>O perioada in care aparatul nu a aputut detecta flux de aer.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8112"/>
        <source>BND</source>
        <translatorcomment>i don`t know what is this BND</translatorcomment>
        <translation>BND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8117"/>
        <source>Timed Breath</source>
        <translation>Respiratie Cronometrata</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8118"/>
        <source>Machine Initiated Breath</source>
        <translation>Respiratie initiata de aparat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8119"/>
        <source>TB</source>
        <translation>TB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation>Utilizator Windows</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation>Utilizez </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation>, am gasit SleepyHead -
</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation>Trebuie sa rulati OSCAR Migration Tool</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="511"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation>&lt;i&gt;Datele din aparatul dvs CPAP vechi ar trebui sa fie restaurate, daca aceasta setarede  Backup nu a fost dezactivata in preferinte cu ocaziua unui import de date.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="444"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Nu am putut lansa Windows Explorer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="445"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Nu am putut gasi explorer.exe in Path pentrua putea lansa  Windows Explorer.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="510"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation>&lt;b&gt;OSCAR pastreaza un backup a datelor de pe SDcard.&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="514"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation>OSCAR nu are inca niciun backup automat pentru acest aparat.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="515"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation>Asta inseamna ca va trebui sa importati datele acestui aparat CPAP din nou ulterior din backup sau de pe cardul SD.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="518"/>
        <source>Important:</source>
        <translation>Important:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="518"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;can not&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation>Dupa ce actualizati versiunea, &lt;font size=+1&gt;nu veti mai putea folosi&lt;/font&gt; acest nume de profil cu versiunea anterioara a OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="519"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation>Daca va faceti griji, alegeti No pentru a iesi si faceti manual backupul Profilului inainte de a reporni OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="520"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation>Sunteti gata sa actualizati la noua versiune programul OSCAR?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="534"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation>Imi pare rau, operatiunea de curatare a esuat, cea ce inseamna ca acesta versiune de OSCAR nu poate porni.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="547"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation>Doriti sa activati Backup automat, astfel incat viitoarea versiune de OSCAR sa poata reconstrui din el baza de date?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="554"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation>OSCAR va porni Importul semiautomat ca sa puteti reinstala datele dvs %1.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="564"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation>OSCAR se va opri si apoi va (incerca sa) porneasca Windows Explorer ca sa puteti face backup manual la Profil:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="566"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation>Folositi managerul de fisiere pentru a face o copie a dosarului de Profil, apoi restartati OSCAR si finalizati actualizarea versiunii.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="523"/>
        <source>Machine Database Changes</source>
        <translation>Schimbari in baza de date a aparatului</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="497"/>
        <source>OSCAR (%1) needs to upgrade its database for %2 %3 %4</source>
        <translation>OSCAR (%1) trebuie sa acualizeze baza de date pentru %2 %3 %4</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="535"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation>Dosarul acestui aparat trebuie sters manual.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="536"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Acest dosar se afla momentan la urmatoarea locatie:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="542"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Reconstruiesc din Backup %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="111"/>
        <source>Couldn&apos;t parse Channels.xml, this build is seriously borked, no choice but to abort!!</source>
        <translation>Nu am putut prelucra fisierul Channels.xml, aceasta versiune de software are probleme, trebuie sa abandonam!</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Therapy Pressure</source>
        <translation>Presiune terapeutica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Inspiratory Pressure</source>
        <translation>Presiune Inspiratorie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Presiune Inspiratorie scazuta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Presiune Inspiratorie mai Mare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Expiratory Pressure</source>
        <translation>Presiune Expiratorie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Presiune Expiratorie scazuta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Presiune Expiratorie mai Mare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support</source>
        <translation>Suport Presiune</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>PS Min</source>
        <translation>PS Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Support Minimum</source>
        <translation>Suport Minim Presiune</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>PS Max</source>
        <translation>PS Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Support Maximum</source>
        <translation>Suport Maxim Presiune</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Min Pressure</source>
        <translation>Presiune Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Presiune Terapeutica Minima</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Max Pressure</source>
        <translation>Presiune Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Presiune Terapueutica Maxima</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Time</source>
        <translation>Timp Ramp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Delay Period</source>
        <translation>Intarzierea Rampei</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Ramp Pressure</source>
        <translation>Presiune Ramp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Starting Ramp Pressure</source>
        <translation>Presiune pornire Ramp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp Event</source>
        <translation>Eveniment Ramp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3150"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3152"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp</source>
        <translation>Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>O perioada anormala de respiratie Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation>Apnee care nu poate fi caracterizata ca fiind nici Centrala nici Obstructiva.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation>O restrictie in respiratie fata de normal,care cauzeaza o aplatizare a valorilorfluxului de aer.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Vibratory Snore (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Mask On Time</source>
        <translation>Timp cu Masca conectata</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Time started according to str.edf</source>
        <translation>Timp incepere conform cu &apos;str.edf&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Summary Only</source>
        <translation>Doar Sumar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>An apnea where the airway is open</source>
        <translation>Apnee in care caile aeriene sunt totusi deschise</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>Apnee cauzata de obstructia cailor aeriene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Hypopnea</source>
        <translation>Hipopnee</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>A partially obstructed airway</source>
        <translation>Obstructie partiala a cailor aeriene</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>Unclassified Apnea</source>
        <translation>Apnee Neclasificata</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>UA</source>
        <translation>UA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Vibratory Snore</source>
        <translation>Sforait Vibrator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>A vibratory snore</source>
        <translation>Un sforait vibrator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation>Un sforait vibrator detectat de un aparat System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7948"/>
        <source>Pressure Pulse</source>
        <translation>Puls Presiune</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7949"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Un puls de presiune fortat pentru a detecta cai aeriene blocate.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation>O scurgere pe langa masca semnificativa afecteaza performanta aparatului.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>Non Responding Event</source>
        <translation>Eveniment fara Raspuns</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Un tip de eveniment respirator care nu raspunde la un puls de presiune suplimnetar.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Expiratory Puff</source>
        <translation>Puls Expirator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Eveniment Intellipap cand ati expirat pe gura.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>Fcilitatea SensAwake va reduce presiunea cand detecteaza trezirea, pentru a facilita readormirea.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>User Flag #1</source>
        <translation>User Flag #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>User Flag #2</source>
        <translation>User Flag #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>User Flag #3</source>
        <translation>User Flag #3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Heart rate in beats per minute</source>
        <translation>Pulsul in bpm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2 %</source>
        <translation>SpO2 %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Saturatia procentuala a oxigenului din sangele periferic</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethysomogram</source>
        <translation>Plethysomograma</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>O fotopletismograma optica care arata ritmul cardiac</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Change</source>
        <translation>Schimbare Puls</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>O schimbare brusca (definibila de utilizator) in frecventa cardiaca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SpO2 Drop</source>
        <translation>SpO2 Drop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>O scadere brusca (definibila de utilizator) in saturatia oxigenului</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SD</source>
        <translation>SD</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Breathing flow rate waveform</source>
        <translation>Graficul fluxului</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <source>L/min</source>
        <translation>L/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure</source>
        <translation>Presiunea pe Masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure (High resolution)</source>
        <translation>Presiunea pe Masca (rezolutie inalta)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Amount of air displaced per breath</source>
        <translation>Volum de aer mobilizat la fiecare respiratie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Graph displaying snore volume</source>
        <translation>Graficul arata volumul sforaitului</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Ventilation</source>
        <translation>Ventilatie Mica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Amount of air displaced per minute</source>
        <translation>Volum de aer mobilizat pe minut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Respiratory Rate</source>
        <translation>Frecventa Respiratiei</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Rate of breaths per minute</source>
        <translation>Respiratii pe minut</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Patient Triggered Breaths</source>
        <translation>Respiratii declansate de pacient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Procent de respiratii declansate de pacient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Respiratii Decl. de Pacient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Leak Rate</source>
        <translation>Rata Scurgeri</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Rate of detected mask leakage</source>
        <translation>Rata scaparilor pe langa masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>I:E Ratio</source>
        <translation>Raport I:E</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Raport intre timpul Inspirator si Expirator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>ratio</source>
        <translation>raport</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Pressure Min</source>
        <translation>Presiune Min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Pressure Max</source>
        <translation>Presiune Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Cheyne Stokes Respiration</source>
        <translation>Respiratie Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>CSR</source>
        <translation>CSR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Periodic Breathing</source>
        <translation>Respratie Periodica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>O perioada anormala de respiratie periodica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Clear Airway</source>
        <translation>Cai aeriene Libere</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Obstructive</source>
        <translation>Obstructiva</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>RERA Trezire Cauzata de Efortul Respirator: o restrictie in respiratie care determina fie o trezire, fie o tulburare a somnului.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Leak Flag</source>
        <translation>Atetionare scurgeri pe langa masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>LF</source>
        <translation>LF</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation>Un eveniment definibil de catre utilizator detectat de procesorul de flux al programului OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perfusion Index</source>
        <translation>Index Perfuzie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>O evaluare relativa a rezistentei pulsului la locul de monitorizare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perf. Index %</source>
        <translation>Index Perf. %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Expiratory Time</source>
        <translation>Timp Expirator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Time taken to breathe out</source>
        <translation>Timp pentru expir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Inspiratory Time</source>
        <translation>Timp Inspirator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Time taken to breathe in</source>
        <translation>Timp pentru inspir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Respiratory Event</source>
        <translation>Eveniment Respirator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>A ResMed data source showing Respiratory Events</source>
        <translation>O sursa de date ResMed care arata Evenimente Respiratorii</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Graficul arata severitatea limitatilor fluxului aerian</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limit.</source>
        <translation>Limita Flux.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Minute Ventilation</source>
        <translation>Mica Ventilatie Tinta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Maximum Leak</source>
        <translation>Scapari Maxime</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>The maximum rate of mask leakage</source>
        <translation>Rata maxima de scapare pe langa masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Max Leaks</source>
        <translation>Scapari Max</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Apnea Hypopnea Index</source>
        <translation>Apnoea Hypopnea Index (AHI)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Graficul arata evolutia AHI in ultima ora</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leak Rate</source>
        <translation>Rata totala de scurgeri</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Pierderile pe langa masca detectate, inclusiv scurgerile naturale din masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leak Rate</source>
        <translation>Rata medie a scaparilor</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median rate of detected mask leakage</source>
        <translation>Media scaparilor detectate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leaks</source>
        <translation>Scapari Medii</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Respiratory Disturbance Index</source>
        <translation>Indice de tulburare respiratorie</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Graficul arata evolutia RDI in ultima ora</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Sleep position in degrees</source>
        <translation>Pozitia somnului in Grade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Upright angle in degrees</source>
        <translation>Unghiul superior in grade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>CPAP Session contains summary data only</source>
        <translation>Sesiunea CPAP contine doar date sumare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="287"/>
        <source>PAP Mode</source>
        <translation>Mod PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="287"/>
        <source>PAP Device Mode</source>
        <translation>Mod dispozitiv PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>APAP (Variable)</source>
        <translation>APAP (Variable)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (EPAP presiune expiratorie Fixa)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="296"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (EPAP presiune expiratorie variabila)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>Height</source>
        <translation>Inaltime</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>Physical Height</source>
        <translation>Inaltime</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Notes</source>
        <translation>Note</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Bookmark Notes</source>
        <translation>Note Semn de carte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Body Mass Index</source>
        <translation>Indice de masa corporala</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Cum va simtiti (0 = rau, 10 = super)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>Bookmark Start</source>
        <translatorcomment>/Semn de carte de inceput</translatorcomment>
        <translation>Inceputul Semnului de carte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Bookmark End</source>
        <translatorcomment>/Semn de carte de final</translatorcomment>
        <translation>Finalul semnului de carte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>Last Updated</source>
        <translation>Ultima actualizate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Journal Notes</source>
        <translation>Note Jurnal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Journal</source>
        <translation>Jurnal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=Treaz 2=REM 3=Somn superficial 4=Somn profund</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Brain Wave</source>
        <translation>Unda cerebrala</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>BrainWave</source>
        <translation>UndaCerebrala</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Awakenings</source>
        <translation>Treziri</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Number of Awakenings</source>
        <translation>Numar de treziri</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Morning Feel</source>
        <translation>Starea la trezire</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>How you felt in the morning</source>
        <translation>Cum v-ati simtit dimineata</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time Awake</source>
        <translation>Timp treaz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time spent awake</source>
        <translation>Timp Treaz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Time In REM Sleep</source>
        <translation>Timp In REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Time spent in REM Sleep</source>
        <translation>Timp in REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Time in REM Sleep</source>
        <translation>Timp in REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Time In Light Sleep</source>
        <translation>Timp In Somn Superficial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Time spent in light sleep</source>
        <translation>Timp in somn superficial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Time in Light Sleep</source>
        <translation>Timp in Somn Superficial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time In Deep Sleep</source>
        <translation>Timp In Somn Adanc</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time spent in deep sleep</source>
        <translation>Timp in somn adanc</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time in Deep Sleep</source>
        <translation>Timp in Somn Adanc</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time to Sleep</source>
        <translation>Tmp pana la adormire</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time taken to get to sleep</source>
        <translation>Timp pentru a adormi</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Zeo masurarea calitatii somnului</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>ZEO ZQ</source>
        <translation>ZEO ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Debugging channel #1</source>
        <translation>Canal depanare #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation>Chestii Super Secrete pe care nu ar trebui sa le vedeti ;)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Test #1</source>
        <translation>Test #1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Debugging channel #2</source>
        <translation>Canal depanare #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Test #2</source>
        <translation>Test #2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="734"/>
        <source>Zero</source>
        <translation>Zero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="737"/>
        <source>Upper Threshold</source>
        <translation>Limita Superioara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="740"/>
        <source>Lower Threshold</source>
        <translation>Prag scazut</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="448"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation>Deoarece nu ati ales un dosar, OSCAR se va opri.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="185"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation>Alegeti dosarul din care doriti importarea datelor programului mai vechi SleepyHead</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="186"/>
        <source>or CANCEL to skip migration.</source>
        <translation>sau ANULATI pentru a opri migrarea.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="200"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation>Dosarul pe care l-ati ales nu contine date SleepyHead valide.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="201"/>
        <source>You cannot use this folder:</source>
        <translation>Nu puteti utiliza acest dosar:</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source>Migrating </source>
        <translation>Transfer </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source> files</source>
        <translation> fisiere</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>from </source>
        <translation>de la </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>to </source>
        <translation>la </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="435"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation>OSCAR va stabili dosarul pentru date.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="436"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation>Daca ati utilizat vechea versiune SleepyHead, OSCAR poate copia vechile date in acest dosar mai tarziu.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="437"/>
        <source>We suggest you use this folder: </source>
        <translation>Sugerez sa folositi acest dosar: </translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="438"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation>Click OK pentru a accepta, sau NO daca doriti sa utilizati un alt dosar.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="444"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation>Alegeti sau creati un nou dosar de date pentru OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="449"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation>Data viitoare cand porniti OSCAR va intreba din nou.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="460"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation>Dosarul pe care l-ati ales nu este gol, dar nici nu contine date OSCAR valide.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="482"/>
        <source>Data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="490"/>
        <source>Migrate SleepyHead Data?</source>
        <translation>Importa datele SleepyHead?</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="491"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation>In fereastra urmatoare alegeti dosarul cu datele SleepyHead pe care doriti sa le importe OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="492"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation>Click [OK] pentru a merge mai departe sau [No] daca nu doriti importarea datelor SleepyHead.</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="550"/>
        <source>The version of OSCAR you just ran is OLDER than the one used to create this data (%1).</source>
        <translation>Versiunea de OSCAR pe care ati lansat-o este mai veche decat cea care a creat acest set de date (%1).</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="552"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>Probabil daca faceti asta datele vor fi corupte, sigur doriti asta?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>Question</source>
        <translation>Intrebare</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="447"/>
        <source>Exiting</source>
        <translation>Oprire</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="461"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Sunteti sigur ca doriti sa utilizati acest dosar?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="261"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation>Amintiti-va sa puneti la loc in aparat cardul SD dupa ce i-ati deblocat scrierea</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="261"/>
        <source>OSCAR Reminder</source>
        <translation>Reamintire OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="432"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation>Puteti lucra cu un singur Profil OSCAR la un moment dat.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="433"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation>Daca salvati datele in cloud, asigurati-va ca OSCAR este inchis si ca sincronizarea cu cloud-ul s-a finalizat inainte de a reporni programul.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="446"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Incarc Profilul &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2141"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation>Imi pare rau, aparatul dvs %1 %2 nu este compatibil momentan cu OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1180"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Sigur doriti resetarea culorilor si setarilor acestui parametru la cele implicite?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1233"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Sigur doriti resetarea culorilor si setarilor graficului acestui parametru la cele implicite?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="39"/>
        <source>There are no graphs visible to print</source>
        <translation>Nu exista grafice vizibile de printat</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="54"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Doriti sa afisez zonele din Semne de carte in acest Raport?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="98"/>
        <source>Printing %1 Report</source>
        <translation>Tiparesc raportul %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="132"/>
        <source>%1 Report</source>
        <translation>%1 Raport</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="190"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 ore, %2 minute, %3 secunde
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="240"/>
        <source>RDI	%1
</source>
        <translation>RDI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="242"/>
        <source>AHI	%1
</source>
        <translation>AHI	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="275"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="281"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="285"/>
        <source>UAI=%1 </source>
        <translation>UAI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="287"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="356"/>
        <source>Reporting from %1 to %2</source>
        <translation>Raportez de la %1 la %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="428"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Graficul fluxului pe intreaga zi</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="434"/>
        <source>Current Selection</source>
        <translation>Selectia curenta</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="444"/>
        <source>Entire Day</source>
        <translation>Toata ziua</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="560"/>
        <source>%1 OSCAR v%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OSCAR v%1</source>
        <translation type="vanished">OSCAR v%1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="568"/>
        <source>Page %1 of %2</source>
        <translation>Pagina %1 din %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation>Zile: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation>Zile cu utilizare redusa: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% compliant, definit ca &gt; %2 ore)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation>(Sesiune: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation>Ora de culcare: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation>Timp trezire: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation>90%</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation>(Doar sumar)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="431"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Exista deja un fisier blocat pentru acest Profil &apos;%1&apos;, creat pe &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="174"/>
        <source>Peak</source>
        <translation>Varf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="178"/>
        <source>%1% %2</source>
        <translation>%1% %2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Fixed Bi-Level</source>
        <translation>Fixat Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="293"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Auto Bi-Level (PS presiune fixa)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Auto Bi-Level (PS presiune variabila)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1466"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1489"/>
        <source>Fixed %1 (%2)</source>
        <translation>Fixat %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1491"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Min %1 Max %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1493"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1508"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1495"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 peste %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1497"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1501"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1499"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation type="unfinished">EPAP %1 PS %2-%3 (%6) {1 ?} {2-%3 ?} {4)?}</translation>
    </message>
    <message>
        <source>EPAP %1 PS %2-%3 (%6)</source>
        <translation type="vanished">EPAP %1 PS %2-%3 (%6)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="165"/>
        <location filename="../oscar/SleepLib/day.cpp" line="167"/>
        <location filename="../oscar/SleepLib/day.cpp" line="169"/>
        <location filename="../oscar/SleepLib/day.cpp" line="174"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="320"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Cele mai recente date oximetrice: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="321"/>
        <source>(last night)</source>
        <translation>(noaptea trecuta)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="322"/>
        <source>(yesterday)</source>
        <translation>(ieri)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="323"/>
        <source>(%2 day ago)</source>
        <translation>(%2 zile in urma)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="328"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Nu au fost importate inca datele pulsoximetrice.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation>Setari SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="404"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="404"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="369"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="369"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="378"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation>Somnopose Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Personal Sleep Coach</source>
        <translation>Antrenor de somn personal</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Baza de date expirata va rugam Reconstruiti datele CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 sec)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation> (%3 sec)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="387"/>
        <source>Pop out Graph</source>
        <translation>Grafic in relief</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1420"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation>Imi pare rau, aparatul dvs CPAP nu inregistreaza date utile pentru a fi afisate in Vizualizare Zilnica :(</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1423"/>
        <source>There is no data to graph</source>
        <translation>Nu exista date pentru a face un grafic</translation>
    </message>
    <message>
        <source>d MMM [ %1 - %2 ]</source>
        <translation type="vanished">d MMM [ %1 - %2 ]</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1608"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2166"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2209"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2280"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2297"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2261"/>
        <source>Hide All Events</source>
        <translation>Ascunde toate evenimentele</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2262"/>
        <source>Show All Events</source>
        <translation>Arata toate Evenimentele</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2603"/>
        <source>Unpin %1 Graph</source>
        <translation>Mobilizeaza raficul %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2605"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2680"/>
        <source>Popout %1 Graph</source>
        <translation>Grafic %1 in relief</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2682"/>
        <source>Pin %1 Graph</source>
        <translation>Pin %1 Graph</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1040"/>
        <source>Plots Disabled</source>
        <translation>Ploturi dezactivate</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1114"/>
        <source>Duration %1:%2:%3</source>
        <translation>Durata %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1115"/>
        <source>AHI %1</source>
        <translation>AHI %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="108"/>
        <source>%1: %2</source>
        <translation>%1: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="151"/>
        <source>Relief: %1</source>
        <translation>Eliberare: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="157"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Ore: %1h, %2m, %3s</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="260"/>
        <source>Machine Information</source>
        <translation>Infoematii Aparat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>Journal Data</source>
        <translation>Date Jurnal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="43"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation>OSCAR a gasit un Jurnal mai vechi, dar se pare ca cineva l-a redenumit intre timp:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="45"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation>OSCAR nu se atinge de acest dosar, ci va crea altul  nou.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="46"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation>Va rog aveti grija cand puneti ceva in dosarul cu Profile al programului OSCAR :-P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="53"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation>Dintr-un motiv oarecare OSCAR nu a putut gasi un jurnal in profilul dvs, dar a gasit mai multe dosare cu jurnale.

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="54"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation>OSCAR a inteles doar prima dintre acestea si va folosi in continuare:

</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="56"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Daca datele dvs vechi lipsesc, copiati intreg continutul celuilalt dosar Journal_XXXXXXX in acesta manual.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation>Mod SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Mod eliberare presiune Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3041"/>
        <source>Ramp Only</source>
        <translation>Doar Ramp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3042"/>
        <source>Full Time</source>
        <translation>Tot timpul</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation>Nivel SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Nivel eliberare presiune Intellipap.</translation>
    </message>
    <message>
        <source>VPAP Adapt</source>
        <translation type="vanished">VPAP Adapt</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1657"/>
        <source>Parsing Identification File</source>
        <translation>Procesez fisierul de identificare</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1759"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Caut fisieruel STR.edf...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1934"/>
        <source>Cataloguing EDF Files...</source>
        <translation>Ordonez fisierele EDF...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1945"/>
        <source>Queueing Import Tasks...</source>
        <translation>Salvez sarcinile de printare...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="2005"/>
        <source>Finishing Up...</source>
        <translation>Finalizare....</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3016"/>
        <source>CPAP Mode</source>
        <translation>Mod CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3022"/>
        <source>VPAP-T</source>
        <translation>VPAP-T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3023"/>
        <source>VPAP-S</source>
        <translation>VPAP-S</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3024"/>
        <source>VPAP-S/T</source>
        <translation>VPAP-S/T</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3025"/>
        <source>??</source>
        <translation>??</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3026"/>
        <source>VPAPauto</source>
        <translation>VPAPauto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3028"/>
        <source>ASVAuto</source>
        <translation>ASVAuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3029"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3030"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3031"/>
        <source>Auto for Her</source>
        <translation>Auto pentru femei</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3034"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3036"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3035"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>Presiune de eliberare la expir ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3043"/>
        <source>Patient???</source>
        <translation>PAcient???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3046"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3048"/>
        <source>EPR Level</source>
        <translation>EPR Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3047"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Nivelul eliberarii presiunii expiratorii</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3051"/>
        <source>0cmH2O</source>
        <translation>0cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3052"/>
        <source>1cmH2O</source>
        <translation>1cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3053"/>
        <source>2cmH2O</source>
        <translation>2cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3054"/>
        <source>3cmH2O</source>
        <translation>3cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3061"/>
        <source>SmartStart</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3062"/>
        <source>Machine auto starts by breathing</source>
        <translation>Aparatul porneste automat la detectarea respiratiei</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3063"/>
        <source>Smart Start</source>
        <translation>Smart Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3070"/>
        <source>Humid. Status</source>
        <translation>Stare Umidificator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3071"/>
        <source>Humidifier Enabled Status</source>
        <translation>Stare Umidificator Activat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3079"/>
        <source>Humid. Level</source>
        <translation>Nivel Umidificator</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3080"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3081"/>
        <source>Humidity Level</source>
        <translation>Nivel Umiditate</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3095"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3097"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3096"/>
        <source>ClimateLine Temperature</source>
        <translation>Temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3102"/>
        <source>Temp. Enable</source>
        <translation>Activare Temp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3103"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>Activeaza Temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3104"/>
        <source>Temperature Enable</source>
        <translation>Activare Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3113"/>
        <source>AB Filter</source>
        <translation>Filtru AB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3114"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3115"/>
        <source>Antibacterial Filter</source>
        <translation>Filtru Antibacterian</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3122"/>
        <source>Pt. Access</source>
        <translation>Pt. Access</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3123"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3124"/>
        <source>Patient Access</source>
        <translation>Acces pacient</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3131"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3132"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3133"/>
        <source>Climate Control</source>
        <translation>Control temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3137"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3136"/>
        <source>Auto</source>
        <translation>Auto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3140"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3142"/>
        <source>Mask</source>
        <translation>Masca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3141"/>
        <source>ResMed Mask Setting</source>
        <translation>Setari masca ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3145"/>
        <source>Pillows</source>
        <translation>Perne</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3146"/>
        <source>Full Face</source>
        <translation>Faciala</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3147"/>
        <source>Nasal</source>
        <translation>Nazala</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3151"/>
        <source>Ramp Enable</source>
        <translation>Activeaza Ramp</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="350"/>
        <source>Snapshot %1</source>
        <translation>Captura %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="228"/>
        <source>%1
Line %2, column %3</source>
        <translation>%1
Linie %2, coloana %3</translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="241"/>
        <source>Could not parse Updates.xml file.</source>
        <translation>Nu am putut prelucra fisierul Updates.xml.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="677"/>
        <source>Loading %1 data for %2...</source>
        <translation>Incarc %1 date pentru %2...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="687"/>
        <source>Scanning Files</source>
        <translation>Scanez fisierele</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="721"/>
        <source>Migrating Summary File Location</source>
        <translation>Importez locatia fisierului Rezumat</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="884"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Incarc Summaries.xml.gz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1015"/>
        <source>Loading Summary Data</source>
        <translation>Incarc Sumarul Datelor</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation>Va rog asteptati...</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation>Varf %1</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="512"/>
        <source>Loading summaries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="165"/>
        <source>Updating Statistics cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="691"/>
        <source>Usage Statistics</source>
        <translation type="unfinished">Statistici utilizare</translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation>%1h %2m</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation>Nu am gasit sesiuni</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="532"/>
        <source>CPAP Statistics</source>
        <translation>Statistici CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="535"/>
        <location filename="../oscar/statistics.cpp" line="1341"/>
        <source>CPAP Usage</source>
        <translation>Utilizare CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="536"/>
        <source>Average Hours per Night</source>
        <translation>Ore pe noapte (medie)</translation>
    </message>
    <message>
        <source>Compliance</source>
        <translation type="vanished">Complianta</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <source>Therapy Efficacy</source>
        <translation>Eficienta terapiei</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="549"/>
        <source>Leak Statistics</source>
        <translation>Statisticile pierderilor din masca</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="554"/>
        <source>Pressure Statistics</source>
        <translation>Statistici presiune</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="568"/>
        <source>Oximeter Statistics</source>
        <translation>Statistici oximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="572"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Saturatia oxigenului in sangele periferic</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="577"/>
        <source>Pulse Rate</source>
        <translation>Pulsul</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="588"/>
        <source>%1 Median</source>
        <translation>%1 Median</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="589"/>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Average %1</source>
        <translation>Media %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="592"/>
        <source>Min %1</source>
        <translation>Min %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="593"/>
        <source>Max %1</source>
        <translation>Max %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="594"/>
        <source>%1 Index</source>
        <translation>%1 Index</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>% of time in %1</source>
        <translation>% din timp in %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="596"/>
        <source>% of time above %1 threshold</source>
        <translation>% din timp peste pragul de %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="597"/>
        <source>% of time below %1 threshold</source>
        <translation>% din timp sub pragul de %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="616"/>
        <source>Name: %1, %2</source>
        <translation>Nume: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="618"/>
        <source>DOB: %1</source>
        <translation>DOB: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="621"/>
        <source>Phone: %1</source>
        <translation>Telefon: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="624"/>
        <source>Email: %1</source>
        <translation>Email: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="627"/>
        <source>Address:</source>
        <translation>Adresa:</translation>
    </message>
    <message>
        <source>Usage Statistics</source>
        <translation type="vanished">Statistici utilizare</translation>
    </message>
    <message>
        <source>This report was generated by OSCAR v%1</source>
        <translation type="vanished">Acest raport a fost generat de OSCAR %1</translation>
    </message>
    <message>
        <source>I can haz data?!?</source>
        <translatorcomment>i have no idea...</translatorcomment>
        <translation type="vanished">I can haz data?!?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1342"/>
        <source>Days Used: %1</source>
        <translation>Zile de utilizare: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1343"/>
        <source>Low Use Days: %1</source>
        <translation>Zile cu utilizare mai redusa: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1344"/>
        <source>Compliance: %1%</source>
        <translation>Complianta: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1368"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation>Zile cu AHI de 5 sau mai mare: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1375"/>
        <source>Best AHI</source>
        <translation>Cel mai bun AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1379"/>
        <location filename="../oscar/statistics.cpp" line="1391"/>
        <source>Date: %1 AHI: %2</source>
        <translation>Data: %1 AHI: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Worst AHI</source>
        <translation>Cel mai prost AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1422"/>
        <source>Best Flow Limitation</source>
        <translation>Cea mai buna limitare de flux aerian</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1426"/>
        <location filename="../oscar/statistics.cpp" line="1439"/>
        <source>Date: %1 FL: %2</source>
        <translation>Data: %1 FL: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1432"/>
        <source>Worst Flow Limtation</source>
        <translation>Cea mai proasta limitare de flux aerian</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1444"/>
        <source>No Flow Limitation on record</source>
        <translation>NU exista limitare de flux inregistrata</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1465"/>
        <source>Worst Large Leaks</source>
        <translation>Cea mai mare pierdere din masca</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1473"/>
        <source>Date: %1 Leak: %2%</source>
        <translation>Data: %1 pierderi din masca: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1479"/>
        <source>No Large Leaks on record</source>
        <translation>NU exista scurgeri mari inregistrate</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1502"/>
        <source>Worst CSR</source>
        <translation>Cel mai prost CSR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1510"/>
        <source>Date: %1 CSR: %2%</source>
        <translation>Data: %1 CSR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1515"/>
        <source>No CSR on record</source>
        <translation>Nu sunt CSR inregistrate</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>Worst PB</source>
        <translation>Cel mai rau PB</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1540"/>
        <source>Date: %1 PB: %2%</source>
        <translation>Data: %1 PB: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1545"/>
        <source>No PB on record</source>
        <translation>Nu exista PB inregistrate</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1553"/>
        <source>Want more information?</source>
        <translation>Doriti mai multe informatii?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1554"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation>OSCAR are nevoie de toate datele sumare incarcate pentru a calcula clele mai bune/mai rele valori pentru fiecare zi in parte.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1555"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation>Va rog activat iPre-Load Summaries checkbox in preferinte ca s ava asigurati ca aceste date vor fi disponibile.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1575"/>
        <source>Best RX Setting</source>
        <translation>Cea mai buna presiune recomandata de medic</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1578"/>
        <location filename="../oscar/statistics.cpp" line="1590"/>
        <source>Date: %1 - %2</source>
        <translation>Data: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1581"/>
        <location filename="../oscar/statistics.cpp" line="1593"/>
        <source>AHI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1582"/>
        <location filename="../oscar/statistics.cpp" line="1594"/>
        <source>Total Hours: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Culminative AHI: %1</source>
        <translation type="vanished">Cel mai mare AHI: %1</translation>
    </message>
    <message>
        <source>Culminative Hours: %1</source>
        <translation type="vanished">Ore de varf: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1587"/>
        <source>Worst RX Setting</source>
        <translation>Cele mai proaste setari recomandate de medic</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1133"/>
        <source>Most Recent</source>
        <translation>Cel mai recent</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="710"/>
        <source>This report was prepared on %1 by OSCAR v%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="713"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation>OSCAR este un software gratuit open-source</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="946"/>
        <source>Changes to Machine Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1032"/>
        <source>No data found?!?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1034"/>
        <source>Oscar has no data to report :(</source>
        <translation>OSCAR nu are date de raportat :(</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1134"/>
        <source>Last Week</source>
        <translation>Ultima saptamana</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1135"/>
        <source>Last 30 Days</source>
        <translation>Ultimele 30 zile</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1136"/>
        <source>Last 6 Months</source>
        <translation>Ultimele 6 luni</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1137"/>
        <source>Last Year</source>
        <translation>Ultimul an</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1141"/>
        <source>Last Session</source>
        <translation>Ultima sesiune</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1182"/>
        <source>Details</source>
        <translation>Detalii</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1196"/>
        <source>No %1 data available.</source>
        <translation>NU exista date %1 disponibile.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1199"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 zi din %2 Date pe %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1205"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 zi din %2 Date, intre %3 si %4</translation>
    </message>
    <message>
        <source>Changes to Prescription Settings</source>
        <translation type="vanished">Schimbari fata de valorile prescrise de medic</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="954"/>
        <source>Days</source>
        <translation>Zile</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="958"/>
        <source>Pressure Relief</source>
        <translation>Eliberare presiune</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="960"/>
        <source>Pressure Settings</source>
        <translation>Setari presiune</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="887"/>
        <source>Machine Information</source>
        <translation>Informatii aparat CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="893"/>
        <source>First Use</source>
        <translation>Prima utilizare</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="894"/>
        <source>Last Use</source>
        <translation>Ultima utilizare</translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation>Actualizare OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation>O noua versiune a $APP este disponibila</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation>Informatii versiune</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation>Note despre versiune</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation>Note despre versiune</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation>&amp;Poate mai tarziu</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation>&amp;Actualizeaza acum</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation>Va rugam asteptati descarcarea si instalarea actualizarii...</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation>Actualizari</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation>Componenta</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation>Versiune</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation>Dimensiune</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation>Progres</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translation>Log</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation>Descarc si in stalez actualizari</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation>&amp;Terminat</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation>Inca nu sunt implementate actualizarile</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation>Caut actualizari pentru OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation>Solicit </translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unvailable for this platform</source>
        <translation>Nu sunt actualizari OSCAR in acest moment</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="510"/>
        <source>OSCAR Updates</source>
        <translation>Actualizari OSCAR</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="459"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation>Versiunea OSCAR %1 este disponibila, deschid link-ul catre site pentru descarcare.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <source>No updates were found for your platform.</source>
        <translation>Nu au fost gasite actualizari pentru sistemul dvs.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="511"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation>Exista actualizari OSCAR disponibile:</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation>%1 bytes primiti</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="462"/>
        <source>You are already running the latest version.</source>
        <translation>Deja aveti ultima versiune de OSCAR.</translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="512"/>
        <source>Would you like to download and install them now?</source>
        <translation>Doriti sa descarcati si sa instalati actualizarile acum?</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation>Formular</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation>Bun venit la OSCAR - Open Source CPAP Analysis Reporter</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation>Ce doriti sa faceti?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation>Importator Date din CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation>Pulsoximetrie semiautomata</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation>Vizualizare zilnica</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation>Privire de ansamblu</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation>Statistici</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap machine.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;ResMed S9 SDCards &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;need &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;to be locked &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;before &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;inserting into your computer&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Some operating systems write cache files which break their special filesystem Journal&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p align=&quot;center&quot;&gt;&lt;span style=&quot; font-size:10pt; font-weight:600;&quot;&gt;Atentie: &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt; SDCardurile din aparatele ResMed S9 &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;trebuie &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;sa fie Read-Only &lt;/span&gt;&lt;span style=&quot; font-size:10pt; font-weight:600; color:#ff0000;&quot;&gt;inainte &lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#ff0000;&quot;&gt;de a le conecta la un PC&lt;br/&gt;&lt;/span&gt;&lt;span style=&quot; font-size:10pt; color:#000000;&quot;&gt;Unele sisteme de operare cum este si Windows modifica structura acestor carduri facandu-le inutilizabile in aparatul CPAP&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation>Ar fi o idee buna sa verificati intai File-&gt;Preferinte</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation>fiindca sunt cateva optiuni care afecteaza importul.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation>Anumite preferinte sunt fortate automat cand este detectat un aparat CPAP ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>Primul import poate dura cateva minute.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>Ultima data cand ati folosit %1...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>noaptea trecuta</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>yesterday</source>
        <translation>ieri</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>%2 days ago</source>
        <translation>%2 zile in urma</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>was %1 (on %2)</source>
        <translation>a fost %1 (%2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="192"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 ore, %2 minute si %3 secunde</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="195"/>
        <source>Your machine was on for %1.</source>
        <translation>Aparatul dvs a functionat pentru %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;Ati avut masca pusa corect %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="208"/>
        <source>under</source>
        <translation>sub</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="209"/>
        <source>over</source>
        <translation>peste</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>reasonably close to</source>
        <translation>destul de aproape de</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>equal to</source>
        <translation>egal cu</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="225"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translatorcomment>i must see the context to accurately translate this</translatorcomment>
        <translation>Ati avut un  AHI de %1, care este %2 pe pozitia %3 in media zilnica din %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="235"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation>Aparatul dvs CPAP a utilizat constant %1 %2 de aer</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="238"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Presiunea dvs a fost sub %1 %2 pentru %3% din timp.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="242"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation>Aparatul dvs a utilizat constant %1-%2 %3 de aer.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="251"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>Presiunea dvs EPAP stabilita la %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="252"/>
        <location filename="../oscar/welcome.cpp" line="258"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Presiunea dvs IPAP a fost sub %1 %2 pentru %3% din timp.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="257"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Presiunea dvs EPAP a fost sub %1 %2 pentru %3% din timp.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="246"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation>Aparatul dvs a fost sub %1-%2 %3 pentru %4% din timp.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Pierderle din masca au fost  %1 %2, care este %3 pe pozitia %4 media zilnica din %5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="284"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>NU au fost inca importate date din CPAP.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="792"/>
        <source>%1 days</source>
        <translation>%1 zile</translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="392"/>
        <source>100% zoom level</source>
        <translation>100% zoom level</translation>
    </message>
    <message>
        <source>Restore X-axis zoom too 100% to view entire days data.</source>
        <translation type="vanished">Restaureaza zoom-ul axei X la 100% pentru a vedea corect toate datele.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="394"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="396"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="398"/>
        <source>Reset Graph Layout</source>
        <translation>Reseteaza graficele</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Reseteaza toate graficele la o inaltime uniforma si in ordinea lor initiala.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="402"/>
        <source>Y-Axis</source>
        <translation>Y-Axis</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="403"/>
        <source>Plots</source>
        <translation>Plots</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="408"/>
        <source>CPAP Overlays</source>
        <translation>Suprapunere CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="411"/>
        <source>Oximeter Overlays</source>
        <translation>Suprapunere pulsoximetrie</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="414"/>
        <source>Dotted Lines</source>
        <translation>Linii punctate</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1801"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1854"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation>Dublu-click pe titlu pentru a fixa sau mobiliza, Click &amp; trage pentru a reaseza graficele in fereastra</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2099"/>
        <source>Remove Clone</source>
        <translation>Elimina clona</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2103"/>
        <source>Clone %1 Graph</source>
        <translation>Cloneaza graficul %1</translation>
    </message>
</context>
</TS>
