<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el_GR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="125"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="239"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="43"/>
        <source>Show data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="47"/>
        <source>About OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="91"/>
        <source>Sorry, could not locate About file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="104"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="116"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="126"/>
        <source>OSCAR v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Important:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="130"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="142"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="878"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="884"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Δεν ήταν δυνατή η λήψη δεδομένων από τον.οξύμετρο.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="478"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Παρακαλώ βεβαιωθείτε ότι έχετε επιλέξει «Ανέβασμα» από το μενού συσκευών οξύμετρο.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="546"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="552"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="435"/>
        <source>Form</source>
        <translation>Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Εμφάνιση ή απόκρυψη του ημερολογίου</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Μετάβαση στην επόμενη μέρα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Μετάβαση στην πιο πρόσφατη ημέρα με τα αρχεία δεδομένων</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Εκδηλώσεις</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Δείτε Μέγεθος</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1404"/>
        <source>Notes</source>
        <translation>Σημειώσεις</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>Ημερολόγιο διάφορων πράξεων</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation>i </translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1066"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1081"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1094"/>
        <source>Color</source>
        <translation>Χρώμα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1116"/>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Small</source>
        <translation>Μικρό</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1131"/>
        <source>Medium</source>
        <translation>Μεσαίο</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1136"/>
        <source>Big</source>
        <translation>Μεγάλο</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1195"/>
        <source>Zombie</source>
        <translation>Βρυκόλακας</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1208"/>
        <source>I&apos;m feeling ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1224"/>
        <source>Weight</source>
        <translation>Βάρος</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1231"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1301"/>
        <source>Awesome</source>
        <translation>Φοβερό</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1339"/>
        <source>B.M.I.</source>
        <translation>Δ Μ Σ.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1355"/>
        <source>Bookmarks</source>
        <translation>Σελιδοδείκτες</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1376"/>
        <source>Add Bookmark</source>
        <translation>Προσθήκη σελιδοδείκτη</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1399"/>
        <source>Starts</source>
        <translation>Αρχή</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1412"/>
        <source>Remove Bookmark</source>
        <translation>Κατάργηση σελιδοδείκτη</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1505"/>
        <source>Flags</source>
        <translation>Σημάδι</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1557"/>
        <source>Graphs</source>
        <translation>Γραφικες Παράστασης</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1582"/>
        <source>Show/hide available graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="283"/>
        <source>Breakdown</source>
        <translation>Ανάλυση</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="283"/>
        <source>events</source>
        <translation>Περιστατικα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="294"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="295"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="358"/>
        <source>Time at Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="602"/>
        <source>No %1 events are recorded this day</source>
        <translation>Αριθμός %1 Περιστατικα που καταγράφικαν αυτή τη μέρα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="722"/>
        <source>%1 event</source>
        <translation>%1 Περιστατικο</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="723"/>
        <source>%1 events</source>
        <translation>%1 Περιστατικα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="766"/>
        <source>Session Start Times</source>
        <translation>Ώρες έναρξης Συνεδρίας</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="767"/>
        <source>Session End Times</source>
        <translation>Ώρες Λήξης Συνεδρίας</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="973"/>
        <source>Session Information</source>
        <translation>Πληροφορίες συνεδρίας</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="996"/>
        <source>Oximetry Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1016"/>
        <source>Duration</source>
        <translation>Διάρκεια</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2278"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="993"/>
        <source>CPAP Sessions</source>
        <translation>Συνεδρίες CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="999"/>
        <source>Sleep Stage Sessions</source>
        <translation>Συνεδρίες Στάδιο Ύπνου</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1002"/>
        <source>Position Sensor Sessions</source>
        <translation>Συνεδρίες θέσης αισθητήρα</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1007"/>
        <source>Unknown Session</source>
        <translation>άγνωστη Συνεδρία</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1077"/>
        <source>Machine Settings</source>
        <translation>Ρυθμίσεις μηχανής</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1083"/>
        <source>Machine Settings Unavailable</source>
        <translation>Ρυθμίσεις μηχανής δεν είναι διαθέσιμη</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1230"/>
        <source>Model %1 - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1235"/>
        <source>PAP Mode: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1239"/>
        <source>(Mode/Pressure settings are guessed on this day.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1350"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1376"/>
        <source>Total ramp time</source>
        <translation>Συνολικός χρόνος ράμπας</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1380"/>
        <source>Time outside of ramp</source>
        <translation>Χρόνος εκτός ράμπας</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1421"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1421"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1458"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1697"/>
        <source>Sorry, this machine only provides compliance data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1716"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Τίποτα δεν είναι εδώ!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1719"/>
        <source>No data is available for this day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1204"/>
        <source>Oximeter Information</source>
        <translation>πληροφορίες Οξύμετρο</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="178"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1034"/>
        <source>Click to %1 this session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1034"/>
        <source>disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1034"/>
        <source>enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1050"/>
        <source>%1 Session #%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1051"/>
        <source>%1h %2m %3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1065"/>
        <source>One or more waveform record(s) for this session had faulty source data. Some waveform overlay points may not match up correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1081"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1208"/>
        <source>SpO2 Desaturations</source>
        <translation>Αποκορεσμούς SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1209"/>
        <source>Pulse Change events</source>
        <translation>Παλμική Αλλαγή γεγονότων</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1210"/>
        <source>SpO2 Baseline Used</source>
        <translation>Χρησιμοποιείται Βάση SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1281"/>
        <source>%1%2</source>
        <translation>%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1276"/>
        <source>Statistics</source>
        <translation>Στατιστικά</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1360"/>
        <source>Total time in apnea</source>
        <translation>Συνολικός χρόνος άπνοιας</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1370"/>
        <source>Time over leak redline</source>
        <translation>Χρόνο πάνω από διαρροή κόκκινη υπογράμμιση</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1635"/>
        <source>BRICK! :(</source>
        <translation>ΤΟΎΒΛΟ! :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1445"/>
        <source>Event Breakdown</source>
        <translation>Κατανομή εκδήλωσης</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1687"/>
        <source>Sessions all off!</source>
        <translation>Συνεδρίες όλοι σβηστοί!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1689"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Υπάρχουν συνεδρίες για αυτήν την ημέρα, αλλά είναι απενεργοποιημένες.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1692"/>
        <source>Impossibly short session</source>
        <translation>Ανέφικτα σύντομη συνεδρίαση</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1693"/>
        <source>Zero hours??</source>
        <translation>Μηδέν ώρες ??</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1696"/>
        <source>BRICK :(</source>
        <translation>ΤΟΥΒΛΟΥ :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1698"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Παραπονεθείτε στον Πάροχο του εξοπλισμό σας!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2014"/>
        <source>Pick a Colour</source>
        <translation>Διαλέξτε χρώμα</translation>
    </message>
    <message>
        <source>This bookmarked is in a currently disabled area..</source>
        <translation type="vanished">Αυτός το στοιχείο είναι σε μια απενεργοποιημένη περιοχή ..</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2321"/>
        <source>Bookmark at %1</source>
        <translation>Αποθηκεύετε με σελιδοδείκτη στο % 1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Εξαγωγή ως CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="60"/>
        <location filename="../oscar/exportcsv.cpp" line="122"/>
        <source>Most Recent Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="125"/>
        <source>Last Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="128"/>
        <source>Last Fortnight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="131"/>
        <source>Last Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="134"/>
        <source>Last 6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="137"/>
        <source>Last Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="119"/>
        <source>Everything</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="108"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="73"/>
        <source>OSCAR_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="76"/>
        <source>Details_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="78"/>
        <source>Sessions_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="80"/>
        <source>Summary_</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="87"/>
        <source>Select file to export to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>CSV Files (*.csv)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="222"/>
        <source>%1% </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="139"/>
        <source>Import Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="140"/>
        <source>The Day records overlap with already existing content.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished">Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="92"/>
        <source>Hide this message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="198"/>
        <source>Search Topic:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="58"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="68"/>
        <source>Help files do not appear to be present.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="84"/>
        <source>HelpEngine did not set up correctly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="99"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="110"/>
        <source>Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="115"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="125"/>
        <source>No documentation available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="213"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="239"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>clear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="165"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="171"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="942"/>
        <source>&amp;Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="996"/>
        <source>Report Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1003"/>
        <location filename="../oscar/mainwindow.ui" line="3243"/>
        <source>Standard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1013"/>
        <source>Monthly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1020"/>
        <source>Date Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1391"/>
        <source>Statistics</source>
        <translation type="unfinished">Στατιστικά</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1441"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1485"/>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1529"/>
        <source>Oximetry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1579"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1629"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2805"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2830"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2834"/>
        <source>&amp;Reset Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2857"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2874"/>
        <source>&amp;Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2878"/>
        <source>&amp;Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2898"/>
        <source>Rebuild CPAP Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2945"/>
        <source>Show Daily view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2956"/>
        <source>Show Overview view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2996"/>
        <source>&amp;Maximize Toggle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2999"/>
        <source>Maximize window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3018"/>
        <source>Reset Graph &amp;Heights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3021"/>
        <source>Reset sizes of graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3086"/>
        <source>Show Right Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3100"/>
        <source>Show Statistics view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3146"/>
        <source>Show &amp;Line Cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3168"/>
        <source>Show Daily Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3185"/>
        <source>Show Daily Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3216"/>
        <source>Report an Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3221"/>
        <source>System Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3232"/>
        <source>Show &amp;Pie Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3235"/>
        <source>Show Pie Chart on Daily page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3238"/>
        <source>Ctrl+P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3246"/>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3251"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3254"/>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2927"/>
        <source>&amp;Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2932"/>
        <source>&amp;Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2988"/>
        <source>&amp;About OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3201"/>
        <source>Show Performance Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3206"/>
        <source>CSV Export Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3211"/>
        <source>Export for Review</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="103"/>
        <source>E&amp;xit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2937"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2942"/>
        <source>View &amp;Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2953"/>
        <source>View &amp;Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2964"/>
        <source>View &amp;Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2972"/>
        <source>-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2983"/>
        <source>Use &amp;AntiAliasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3010"/>
        <source>Show Debug Pane</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3026"/>
        <source>Take &amp;Screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3034"/>
        <source>O&amp;ximetry Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3042"/>
        <source>Print &amp;Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3047"/>
        <source>&amp;Edit Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3182"/>
        <source>Daily Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3193"/>
        <source>Backup &amp;Journal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3052"/>
        <source>Online Users &amp;Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3057"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3062"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3067"/>
        <source>Change &amp;User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3072"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3083"/>
        <source>Right &amp;Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3165"/>
        <source>Daily Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3094"/>
        <source>View S&amp;tatistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1320"/>
        <source>Navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="441"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1751"/>
        <source>Bookmarks</source>
        <translation type="unfinished">Σελιδοδείκτες</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2746"/>
        <source>Records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2888"/>
        <source>Purge ALL CPAP Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2809"/>
        <source>Exp&amp;ort Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1347"/>
        <source>Profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2882"/>
        <source>Purge Oximetry Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2919"/>
        <source>&amp;Import SDcard Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3097"/>
        <source>View Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3108"/>
        <source>Import &amp;ZEO Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3113"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3118"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3123"/>
        <source>Change &amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3128"/>
        <source>Change &amp;Data Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3133"/>
        <source>Import &amp;Somnopose Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="3138"/>
        <source>Current Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="530"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="509"/>
        <location filename="../oscar/mainwindow.cpp" line="2236"/>
        <source>Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="100"/>
        <source>&amp;About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="715"/>
        <location filename="../oscar/mainwindow.cpp" line="1926"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="660"/>
        <source>Import Problem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="816"/>
        <source>Please insert your CPAP data card...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="889"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="928"/>
        <source>CPAP Data Located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="957"/>
        <source>Please remember to point the importer at the root folder or drive letter of your data-card, and not a subfolder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="958"/>
        <source>Import Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1021"/>
        <source>Processing import list...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1042"/>
        <source>Importing Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1392"/>
        <source>Updates are not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1505"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1513"/>
        <source>The FAQ is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1761"/>
        <location filename="../oscar/mainwindow.cpp" line="1788"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1901"/>
        <source>Are you sure you want to rebuild all CPAP data for the following machine:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1911"/>
        <source>For some reason, OSCAR does not have any backups for the following machine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1966"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s machine database for the following machine:&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2023"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2073"/>
        <source>No help is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2317"/>
        <source>Donations are not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2527"/>
        <source>%1&apos;s Journal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2529"/>
        <source>Choose where to save journal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2529"/>
        <source>XML Files (*.xml)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2549"/>
        <source>Export review is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2564"/>
        <source>Reporting issues is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2573"/>
        <source>OSCAR Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="245"/>
        <source>Help Browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1331"/>
        <source>Please open a profile first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1913"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1914"/>
        <source>Are you really sure you want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1929"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1930"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1968"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1969"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2309"/>
        <source>The Glossary will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2457"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2459"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2480"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="320"/>
        <source>%1 %2</source>
        <translation type="unfinished">%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="491"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="656"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="656"/>
        <source>Import Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="658"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="658"/>
        <source>Up to date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="660"/>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="823"/>
        <source>Choose a folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="885"/>
        <source>No profile has been selected for Import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="893"/>
        <source>Import is already running in the background.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="921"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="923"/>
        <source>A %1 file structure was located at:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="929"/>
        <source>Would you like to import from this location?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="932"/>
        <source>Specify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1336"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1442"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1444"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1760"/>
        <location filename="../oscar/mainwindow.cpp" line="1787"/>
        <source>Gah!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1904"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2268"/>
        <source>There was a problem opening ZEO File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2272"/>
        <source>Zeo CSV Import complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2294"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2298"/>
        <source>MSeries Import complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2355"/>
        <source>There was a problem opening Somnopose Data File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2359"/>
        <source>Somnopause Data Import complete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2019"/>
        <source>Auto-Fit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2020"/>
        <source>Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2021"/>
        <source>Override</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2022"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2028"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2029"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2064"/>
        <source>Scaling Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2086"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="70"/>
        <source>I agree to all the conditions above.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="111"/>
        <source>User Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="137"/>
        <source>User Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="152"/>
        <source>Keep the kids out.. Nothing more.. This isn&apos;t meant to be uber security.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Password Protect Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="182"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="196"/>
        <source>...twice...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="213"/>
        <source>Locale Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="285"/>
        <source>Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="253"/>
        <source>TimeZone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="240"/>
        <source>DST Zone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="320"/>
        <source>Personal Information (for reports)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="344"/>
        <source>First Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="354"/>
        <source>Last Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="364"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>D.O.B.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="383"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>Gender</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="399"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="404"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="420"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="459"/>
        <source>Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="464"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="477"/>
        <source>Contact Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="504"/>
        <location filename="../oscar/newprofile.ui" line="779"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="521"/>
        <location filename="../oscar/newprofile.ui" line="810"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="531"/>
        <location filename="../oscar/newprofile.ui" line="800"/>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="576"/>
        <source>CPAP Treatment Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="603"/>
        <source>Date Diagnosed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="617"/>
        <source>Untreated AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="631"/>
        <source>CPAP Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="639"/>
        <source>CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="644"/>
        <source>APAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="649"/>
        <source>Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="654"/>
        <source>ASV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="662"/>
        <source>RX Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="708"/>
        <source>Doctors / Clinic Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="735"/>
        <source>Doctors Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="752"/>
        <source>Practice Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="762"/>
        <source>Patient ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="877"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="955"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="971"/>
        <source>&amp;Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="987"/>
        <location filename="../oscar/newprofile.cpp" line="274"/>
        <location filename="../oscar/newprofile.cpp" line="283"/>
        <source>&amp;Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="61"/>
        <source>Select Country</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="109"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="120"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="129"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="114"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="117"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="125"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="132"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019 Nightowl Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="155"/>
        <source>Please provide a username for this profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="165"/>
        <source>Passwords don&apos;t match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Profile Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="195"/>
        <source>Accept and save this information?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="272"/>
        <source>&amp;Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="448"/>
        <source>&amp;Close this window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="130"/>
        <source>Start:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="153"/>
        <source>End:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="176"/>
        <source>Reset view to selected date range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="195"/>
        <location filename="../oscar/overview.ui" line="244"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="225"/>
        <source>Toggle Graph Visibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="260"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="267"/>
        <source>Graphs</source>
        <translation type="unfinished">Γραφικες Παράστασης</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="183"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="185"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="191"/>
        <source>Usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="191"/>
        <source>Usage
(hours)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="194"/>
        <source>Session Times</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="202"/>
        <source>Total Time in Apnea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="202"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="237"/>
        <source>Body
Mass
Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="238"/>
        <source>How you felt
(0-10)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="576"/>
        <source>Show all graphs</source>
        <translation type="unfinished">Εμφάνιση όλων των γραφικoν παραστάσεον</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="589"/>
        <source>Hide all graphs</source>
        <translation type="unfinished">Απόκρυψη όλων των γραφικoν παραστάσεον</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="37"/>
        <source>Oximeter Import Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="768"/>
        <source>Skip this page next time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="866"/>
        <source>Where would you like to import from?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="921"/>
        <source>CMS50Fv3.7+/H/I, CMS50D+v4.6, Pulox PO-400/500</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="967"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1004"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1036"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1077"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1106"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1154"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1193"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1272"/>
        <source>Please connect your oximeter device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1290"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1319"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1345"/>
        <source>Press Start to commence recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1391"/>
        <source>Show Live Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1422"/>
        <location filename="../oscar/oximeterimport.ui" line="1676"/>
        <source>Duration</source>
        <translation type="unfinished">Διάρκεια</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1458"/>
        <source>SpO2 %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1540"/>
        <source>Pulse Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1632"/>
        <source>Multiple Sessions Detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1671"/>
        <source>Start Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1681"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1698"/>
        <source>Import Completed. When did the recording start?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1722"/>
        <source>Day recording (normally would of) started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1768"/>
        <source>Oximeter Starting time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1780"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1796"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1857"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1878"/>
        <source>Choose CPAP session to sync to:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1917"/>
        <location filename="../oscar/oximeterimport.ui" line="1956"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1998"/>
        <source>You can manually adjust the time here if required:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2019"/>
        <source>HH:mm:ssap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2116"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2097"/>
        <source>&amp;Information Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="823"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;Make sure your correct oximeter type is selected otherwise import will fail.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="910"/>
        <source>Select Oximeter Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="926"/>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="931"/>
        <source>ChoiceMMed MD300W1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1007"/>
        <source>Set device date/time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1014"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1017"/>
        <source>Set device identifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1080"/>
        <source>Erase session after successful upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1112"/>
        <source>Import directly from a recording on a device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1237"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1648"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1825"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2135"/>
        <source>&amp;Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2154"/>
        <source>&amp;Choose Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2173"/>
        <source>&amp;End Recording</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2192"/>
        <source>&amp;Sync and Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2211"/>
        <source>&amp;Save and Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="2230"/>
        <source>&amp;Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="188"/>
        <source>Scanning for compatible oximeters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="220"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="228"/>
        <source>Connecting to %1 Oximeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="256"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="259"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="302"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="327"/>
        <source>Nothing to import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="332"/>
        <source>Waiting for %1 to start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="335"/>
        <source>Select upload option on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="336"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="367"/>
        <source>%1 device is uploading data...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="368"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="387"/>
        <source>Oximeter import completed..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Select a valid oximetry data file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="411"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="433"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="478"/>
        <source>Live Oximetry Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="530"/>
        <source>Live Oximetry Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="531"/>
        <source>Live Oximetry import has been stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1091"/>
        <source>Oximeter Session %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1136"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1147"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1150"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="460"/>
        <source>Oximeter not detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="467"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="481"/>
        <source>Starting up...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="482"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="529"/>
        <source>Live Import Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="582"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="586"/>
        <source>No CPAP data available on %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="594"/>
        <source>%1</source>
        <translation type="unfinished">%1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="721"/>
        <source>Recording...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="728"/>
        <source>Finger not detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="828"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="831"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="843"/>
        <source>Something went wrong getting session data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1132"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1134"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1138"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1140"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>Please remember:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1149"/>
        <source>Important Notes:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="160"/>
        <source>SpO2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="346"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="23"/>
        <source>Preferences</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="64"/>
        <source>&amp;Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="154"/>
        <source>Combine Close Sessions </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="164"/>
        <location filename="../oscar/preferencesdialog.ui" line="249"/>
        <location filename="../oscar/preferencesdialog.ui" line="732"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="184"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="239"/>
        <source>Ignore Short Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="266"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="310"/>
        <source>Day Split Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="320"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="393"/>
        <source>Session Storage Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="440"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="638"/>
        <source>&amp;CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1268"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1271"/>
        <source> hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="946"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1024"/>
        <source>Flow Restriction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1065"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="988"/>
        <location filename="../oscar/preferencesdialog.ui" line="1069"/>
        <location filename="../oscar/preferencesdialog.ui" line="1546"/>
        <location filename="../oscar/preferencesdialog.ui" line="1706"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1042"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1086"/>
        <source>Duration of airflow restriction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="978"/>
        <location filename="../oscar/preferencesdialog.ui" line="1089"/>
        <location filename="../oscar/preferencesdialog.ui" line="1563"/>
        <location filename="../oscar/preferencesdialog.ui" line="1651"/>
        <location filename="../oscar/preferencesdialog.ui" line="1680"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1125"/>
        <source>Event Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1112"/>
        <source>Allow duplicates near machine events.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1190"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1194"/>
        <source> minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1233"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1236"/>
        <source>Zero Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="685"/>
        <source>CPAP Clock Drift</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="501"/>
        <source>Do not import sessions older than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="508"/>
        <source>Sessions older than this date will not be imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="534"/>
        <source>dd MMMM yyyy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1249"/>
        <source>User definable threshold considered large leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1252"/>
        <source> L/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1216"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1793"/>
        <location filename="../oscar/preferencesdialog.ui" line="1865"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1489"/>
        <source>&amp;Oximetry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1079"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1102"/>
        <source>#1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1008"/>
        <source>#2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1001"/>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1641"/>
        <source>SPO2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1703"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1696"/>
        <source>Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1661"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1553"/>
        <location filename="../oscar/preferencesdialog.ui" line="1583"/>
        <location filename="../oscar/preferencesdialog.ui" line="1664"/>
        <source> bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1648"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1677"/>
        <source>Minimum duration of pulse change event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1560"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1921"/>
        <source>&amp;General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1316"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1319"/>
        <source>Preferred Calculation Methods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1348"/>
        <source>Middle Calculations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1362"/>
        <source>Upper Percentile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="91"/>
        <source>Session Splitting Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="356"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="359"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="568"/>
        <source>Memory and Startup Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="610"/>
        <source>Pre-Load all summary data at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="597"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="600"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="624"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="627"/>
        <source>Import without asking for confirmation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1154"/>
        <source>General CPAP and Related Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1163"/>
        <source>Enable Unknown Events Channels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1294"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1299"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1170"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1226"/>
        <source>Preferred major event index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1177"/>
        <source>Compliance defined as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1219"/>
        <source>Flag leaks over threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="766"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="712"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="759"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1325"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1376"/>
        <source>Median is recommended for ResMed users.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1380"/>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1385"/>
        <source>Weighted Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1390"/>
        <source>Normal Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1414"/>
        <source>True Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1419"/>
        <source>99% Percentile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1355"/>
        <source>Maximum Calcs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1950"/>
        <source>General Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2677"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2680"/>
        <source>Skip over Empty Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1971"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1975"/>
        <source>Enable Multithreading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="574"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="483"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1410"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1428"/>
        <source>Combined Count divided by Total Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1433"/>
        <source>Time Weighted average of Indice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1438"/>
        <source>Standard average of indice</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1369"/>
        <source>Culminative Indices</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="951"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1770"/>
        <source>Events</source>
        <translation type="unfinished">Εκδηλώσεις</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1816"/>
        <location filename="../oscar/preferencesdialog.ui" line="1895"/>
        <source>Reset &amp;Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1829"/>
        <location filename="../oscar/preferencesdialog.ui" line="1908"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1842"/>
        <source>Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1629"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1540"/>
        <source>Other oximetry options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1590"/>
        <source>Flag SPO2 Desaturations Below</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1573"/>
        <source>Discard segments under</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1610"/>
        <source>Flag Pulse Rate Above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1600"/>
        <source>Flag Pulse Rate Below</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="432"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="452"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="462"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="467"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="474"/>
        <source>This maintains a backup of SD-card data for ResMed machines, 

ResMed S9 series machines delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="607"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="782"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="789"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="885"/>
        <source>4 cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="895"/>
        <source>20 cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="927"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="998"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve machine detected event positioning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1160"/>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1982"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2014"/>
        <source>Automatically Check For Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2031"/>
        <source>Check for new version every</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2038"/>
        <source>Sourceforge hosts this project for free.. Please be considerate of their resources..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2054"/>
        <source>days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2119"/>
        <source>&amp;Check for Updates now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2142"/>
        <source>Last Checked For Updates: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2155"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2177"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If your interested in helping test new features and bugfixes early, click here.&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;But please be warned this will sometimes mean breaky code..&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2186"/>
        <source>I want to try experimental and test builds (Advanced users only please.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2210"/>
        <source>&amp;Appearance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2239"/>
        <source>Graph Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2255"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2513"/>
        <source>Bar Tops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2518"/>
        <source>Line Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2608"/>
        <source>Overview Linecharts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2729"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2553"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2447"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2435"/>
        <source>Scroll Dampening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2425"/>
        <source>Tooltip Timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2532"/>
        <source>Default display height of graphs in pixels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2505"/>
        <source>Graph Tooltips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2381"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2386"/>
        <source>Standard Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2391"/>
        <source>Top Markers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2339"/>
        <source>Graph Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="422"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="577"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="617"/>
        <source>Automatically load last used profile on start-up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="798"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="854"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1457"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1510"/>
        <source>Oximetry Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1750"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method does &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP machine, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2245"/>
        <source>On Opening</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2258"/>
        <location filename="../oscar/preferencesdialog.ui" line="2262"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2267"/>
        <location filename="../oscar/preferencesdialog.ui" line="2306"/>
        <source>Welcome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2272"/>
        <location filename="../oscar/preferencesdialog.ui" line="2311"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2282"/>
        <location filename="../oscar/preferencesdialog.ui" line="2321"/>
        <source>Statistics</source>
        <translation type="unfinished">Στατιστικά</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2290"/>
        <source>Switch Tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2301"/>
        <source>No change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2329"/>
        <source>After Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2355"/>
        <source>Overlay Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2365"/>
        <source>Line Thickness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2401"/>
        <source>The pixel thickness of line plots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2627"/>
        <source>Other Visual Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2633"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2640"/>
        <source>Use Anti-Aliasing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2647"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2650"/>
        <source>Square Wave Plots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2657"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2660"/>
        <source>Use Pixmap Caching</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2667"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2670"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2687"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2690"/>
        <source>Allow YAxis Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2697"/>
        <source>Whether to include machine serial number on machine settings changes report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2700"/>
        <source>Include Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2723"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2754"/>
        <source>Fonts (Application wide settings)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2789"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2808"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2827"/>
        <source>Bold  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2849"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2862"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2926"/>
        <source>Graph Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2987"/>
        <source>Graph Titles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3048"/>
        <source>Big  Text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3115"/>
        <location filename="../oscar/preferencesdialog.cpp" line="444"/>
        <location filename="../oscar/preferencesdialog.cpp" line="576"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3159"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3166"/>
        <source>&amp;Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="439"/>
        <location filename="../oscar/preferencesdialog.cpp" line="570"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="440"/>
        <location filename="../oscar/preferencesdialog.cpp" line="571"/>
        <source>Color</source>
        <translation type="unfinished">Χρώμα</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="442"/>
        <source>Flag Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="443"/>
        <location filename="../oscar/preferencesdialog.cpp" line="575"/>
        <source>Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="460"/>
        <source>CPAP Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="461"/>
        <source>Oximeter Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="462"/>
        <source>Positional Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="463"/>
        <source>Sleep Stage Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="464"/>
        <source>Unknown Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="636"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="514"/>
        <location filename="../oscar/preferencesdialog.cpp" line="643"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="65"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; machines due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed machines, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="299"/>
        <location filename="../oscar/preferencesdialog.cpp" line="300"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1263"/>
        <location filename="../oscar/preferencesdialog.cpp" line="1268"/>
        <source>%1 %2</source>
        <translation type="unfinished">%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2277"/>
        <location filename="../oscar/preferencesdialog.ui" line="2316"/>
        <location filename="../oscar/preferencesdialog.cpp" line="441"/>
        <location filename="../oscar/preferencesdialog.cpp" line="572"/>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="506"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="519"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="529"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="534"/>
        <location filename="../oscar/preferencesdialog.cpp" line="667"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="540"/>
        <location filename="../oscar/preferencesdialog.cpp" line="673"/>
        <source>This is a description of what this channel does.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="573"/>
        <source>Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="574"/>
        <source>Upper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="593"/>
        <source>CPAP Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="594"/>
        <source>Oximeter Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="595"/>
        <source>Positional Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="596"/>
        <source>Sleep Stage Waveforms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="652"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="657"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="662"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="772"/>
        <source>Data Processing Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="773"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="781"/>
        <source>Data Reindex Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="782"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="788"/>
        <source>Restart Required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="789"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1145"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1146"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1147"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1191"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1192"/>
        <source>Are you really sure you want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="49"/>
        <source>Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Minor Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Span</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Always Minor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="272"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1143"/>
        <source>This may not be a good idea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1144"/>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileSelect</name>
    <message>
        <location filename="../oscar/profileselect.ui" line="14"/>
        <source>Select Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="115"/>
        <source>Search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="191"/>
        <source>Start with the selected user profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="194"/>
        <source>&amp;Select User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="215"/>
        <source>Create a new user profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="218"/>
        <source>New Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="235"/>
        <source>Choose a different OSCAR data folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="274"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="310"/>
        <source>Click here if you didn&apos;t want to start OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="383"/>
        <source>The current location of OSCAR data store.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="241"/>
        <source>&amp;Different Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="287"/>
        <source>[version]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="313"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="370"/>
        <source>Folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.ui" line="389"/>
        <source>[data directory]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="92"/>
        <source>Open Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="93"/>
        <source>Edit Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="95"/>
        <source>Delete Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="139"/>
        <location filename="../oscar/profileselect.cpp" line="220"/>
        <source>Enter Password for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="158"/>
        <location filename="../oscar/profileselect.cpp" line="342"/>
        <source>Incorrect Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="160"/>
        <source>You entered the password wrong too many times.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>Enter the word DELETE below to confirm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="182"/>
        <source>You are about to destroy profile &apos;%1&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>Sorry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="201"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="239"/>
        <source>You entered an incorrect password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="242"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to delete it manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="255"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="259"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="289"/>
        <source>Create new profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="326"/>
        <source>Enter Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselect.cpp" line="345"/>
        <source>You entered an Incorrect Password too many times. Exiting!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="55"/>
        <source>...</source>
        <translation type="unfinished">...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="184"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="199"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="216"/>
        <source>&amp;Open Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="227"/>
        <source>&amp;Edit Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="241"/>
        <source>&amp;New Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="260"/>
        <source>Profile: None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="281"/>
        <source>Please select or create a profile...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="332"/>
        <source>Destroy Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="89"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Ventilator Brand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Other Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Last Imported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="134"/>
        <location filename="../oscar/profileselector.cpp" line="311"/>
        <source>%1, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="169"/>
        <source>You must create a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="232"/>
        <location filename="../oscar/profileselector.cpp" line="362"/>
        <source>Enter Password for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="248"/>
        <location filename="../oscar/profileselector.cpp" line="381"/>
        <source>You entered an incorrect password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="251"/>
        <source>Forgot your password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="251"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="321"/>
        <source>Select a profile first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="384"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="394"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="394"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="394"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="412"/>
        <source>DELETE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="413"/>
        <source>Sorry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="413"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="426"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="430"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>KB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>MB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>GB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>TB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="440"/>
        <source>PB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="460"/>
        <source>Summaries:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="461"/>
        <source>Events:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="462"/>
        <source>Backups:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="474"/>
        <location filename="../oscar/profileselector.cpp" line="514"/>
        <source>Hide disk usage information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="477"/>
        <source>Show disk usage information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="495"/>
        <source>Name: %1, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="498"/>
        <source>Phone: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="501"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="504"/>
        <source>Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="507"/>
        <source>No profile information given</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="510"/>
        <source>Profile: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="56"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1246"/>
        <source>No Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="366"/>
        <source>Events</source>
        <translation type="unfinished">Εκδηλώσεις</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="364"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="367"/>
        <source>Duration</source>
        <translation type="unfinished">Διάρκεια</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="381"/>
        <source>(% %1 in events)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Feb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Mar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Apr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>May</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="65"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="397"/>
        <source>Jun</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jul</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Aug</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Sep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Oct</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Nov</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Dec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>ft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="664"/>
        <source>lb</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source>oz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source>Kg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="198"/>
        <source>Med.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="216"/>
        <source>Min: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="247"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="257"/>
        <source>Min: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="252"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="262"/>
        <source>Max: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="266"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="270"/>
        <source>%1: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="274"/>
        <source>???: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="281"/>
        <source>Max: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="287"/>
        <source>%1 (%2 days): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="289"/>
        <source>%1 (%2 day): </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="347"/>
        <source>% in %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="353"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="680"/>
        <location filename="../oscar/SleepLib/common.cpp" line="668"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="359"/>
        <source>Min %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="665"/>
        <source>
Hours: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="731"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="812"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="931"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="933"/>
        <source>Mask On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="933"/>
        <source>Mask Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="944"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1115"/>
        <source>TTIA:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1128"/>
        <source>
TTIA: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1229"/>
        <source>%1 %2 / %3 / %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>h</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <source>m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="675"/>
        <source>Events/hr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>Hz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <source>bpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>Litres</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="681"/>
        <source>ml</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <source>Breaths/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>Severity (0-1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>Degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="692"/>
        <source>Busy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>Please Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Compliance Only :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>Graphs Switched Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Summary Only :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="699"/>
        <source>Sessions Switched Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>&amp;Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>&amp;No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="705"/>
        <source>&amp;Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="706"/>
        <source>&amp;Destroy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="707"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>BMI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>Weight</source>
        <translation type="unfinished">Βάρος</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Zombie</source>
        <translation type="unfinished">Βρυκόλακας</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="712"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Pulse Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="713"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <source>Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="718"/>
        <source>Profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>Oximetry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Oximeter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="723"/>
        <source>Event Flags</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="726"/>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="729"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3020"/>
        <source>CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="730"/>
        <source>BiPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <source>Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="732"/>
        <source>EPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="733"/>
        <source>Min EPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <source>Max EPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <source>IPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <source>Min IPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <source>Max IPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3021"/>
        <source>APAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3027"/>
        <source>ASV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7966"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <source>ST/ASV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <source>Humidifier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>OA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="747"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>CA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="749"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>FL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="750"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <source>LE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>EP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="753"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>VS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>VS2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RERA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="757"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7950"/>
        <source>PP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <source>P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>RE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="760"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>NR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <source>NRI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <source>O2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>PC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>UF1</source>
        <translation type="unfinished">UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>UF2</source>
        <translation type="unfinished">UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>UF3</source>
        <translation type="unfinished">UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <source>PS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>RDI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <source>AI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <source>HI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="773"/>
        <source>UAI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <source>CAI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <source>FLI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <source>REI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <source>EPI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <source>ÇSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>PB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>IE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Insp. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Exp. Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="787"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Resp. Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="788"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="789"/>
        <source>Flow Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <source>Pat. Trig. Breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <source>Tgt. Min. Vent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Vent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="794"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Vent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Tidal Volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Resp. Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>Leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <source>Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>Large Leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>LL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <source>Unintentional Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>MaskPressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Flow Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Sleep Stage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <source>Usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="808"/>
        <source>Sessions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="809"/>
        <source>Pr. Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>No Data Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="239"/>
        <source>Built with Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="242"/>
        <source>Branch:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="244"/>
        <source>Revision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="246"/>
        <source>App key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="248"/>
        <source>Operating system:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="249"/>
        <source>Graphics Engine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="250"/>
        <source>Graphics Engine type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="656"/>
        <source>Software Engine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="657"/>
        <source>ANGLE / OpenGLES</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="658"/>
        <source>Desktop OpenGL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="660"/>
        <source> m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source> cm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <source>Bookmarks</source>
        <translation type="unfinished">Σελιδοδείκτες</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <source>v%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3015"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3017"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="816"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>Brand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="818"/>
        <source>Serial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="819"/>
        <source>Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="820"/>
        <source>Machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <source>Channel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Inclination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Orientation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <source>DOB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="829"/>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <source>Patient ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="833"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>Bedtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Wake-up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>Mask Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="124"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>Ready</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <source>First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="843"/>
        <source>Last</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="845"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="846"/>
        <source>On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="847"/>
        <source>Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="848"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7997"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7998"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <source>Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <source>Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <source>Med</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="856"/>
        <source>Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="202"/>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <source>Avg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="200"/>
        <location filename="../oscar/SleepLib/common.cpp" line="858"/>
        <source>W-Avg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="772"/>
        <source>Non Data Capable Machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="773"/>
        <source>Your Philips Respironics CPAP machine (Model %1) is unfortunately not a data capable model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="670"/>
        <source>Getting Ready...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="785"/>
        <source>Machine Unsupported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="786"/>
        <source>Sorry, your Philips Respironics CPAP machine (Model %1) is not supported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="774"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this machine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="691"/>
        <source>Scanning Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="700"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1996"/>
        <source>Importing Sessions...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="705"/>
        <source>Finishing up...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="787"/>
        <source>The developers needs a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make it work with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="806"/>
        <source>Machine Untested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="807"/>
        <source>Your Philips Respironics CPAP machine (Model %1) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="808"/>
        <source>It seems similar enough to other machines that it might work, but the developers would like a .zip copy of this machine&apos;s SD card and matching Encore .pdf reports to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="4070"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="6875"/>
        <source>%1mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8042"/>
        <source>15mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8041"/>
        <source>22mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7954"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7956"/>
        <source>Flex Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7955"/>
        <source>PRS1 pressure relief mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7961"/>
        <source>C-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7962"/>
        <source>C-Flex+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7963"/>
        <source>A-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7964"/>
        <source>Rise Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7965"/>
        <source>Bi-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7970"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7972"/>
        <source>Flex Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7971"/>
        <source>PRS1 pressure relief setting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7976"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8007"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8029"/>
        <source>x1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7977"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8008"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8030"/>
        <source>x2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7978"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8009"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8031"/>
        <source>x3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7979"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8010"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8032"/>
        <source>x4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7980"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8011"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8033"/>
        <source>x5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7984"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7986"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3072"/>
        <source>Humidifier Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7985"/>
        <source>PRS1 humidifier connected?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7988"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7989"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7993"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7995"/>
        <source>Heated Tubing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7994"/>
        <source>Heated Tubing Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8002"/>
        <source>Humidification Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8003"/>
        <source>PRS1 Humidification level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8004"/>
        <source>Humid. Lvl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8015"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8016"/>
        <source>System One Resistance Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8017"/>
        <source>Sys1 Resist. Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8024"/>
        <source>System One Resistance Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8025"/>
        <source>System One Mask Resistance Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8026"/>
        <source>Sys1 Resist. Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8037"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8039"/>
        <source>Hose Diameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8038"/>
        <source>Diameter of primary CPAP hose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8046"/>
        <source>System One Resistance Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8047"/>
        <source>Whether System One resistance settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8048"/>
        <source>Sys1 Resist. Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8055"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8057"/>
        <source>Auto On</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8056"/>
        <source>A few breaths automatically starts machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8064"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8066"/>
        <source>Auto Off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8065"/>
        <source>Machine automatically switches off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8073"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8075"/>
        <source>Mask Alert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8074"/>
        <source>Whether or not machine allows Mask checking.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8082"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8084"/>
        <source>Show AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8083"/>
        <source>Whether or not machine shows AHI via LCD panel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8096"/>
        <source>Unknown PRS1 Code %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8097"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8098"/>
        <source>PRS1_%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8110"/>
        <source>Breathing Not Detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8111"/>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8112"/>
        <source>BND</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8117"/>
        <source>Timed Breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8118"/>
        <source>Machine Initiated Breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="8119"/>
        <source>TB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="36"/>
        <source>Windows User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>Using </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="200"/>
        <source>, found SleepyHead -
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="201"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="511"/>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="444"/>
        <source>Launching Windows Explorer failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="445"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="510"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="514"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="515"/>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="518"/>
        <source>Important:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="518"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;can not&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="519"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="520"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="534"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="547"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="554"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="564"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="566"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="523"/>
        <source>Machine Database Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="497"/>
        <source>OSCAR (%1) needs to upgrade its database for %2 %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="535"/>
        <source>The machine data folder needs to be removed manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="536"/>
        <source>This folder currently resides at the following location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="542"/>
        <source>Rebuilding from %1 Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="111"/>
        <source>Couldn&apos;t parse Channels.xml, this build is seriously borked, no choice but to abort!!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="123"/>
        <source>Therapy Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="124"/>
        <source>Inspiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="125"/>
        <source>Lower Inspiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="126"/>
        <source>Higher Inspiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="127"/>
        <source>Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="128"/>
        <source>Lower Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="129"/>
        <source>Higher Expiratory Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="130"/>
        <source>Pressure Support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>PS Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="131"/>
        <source>Pressure Support Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>PS Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="132"/>
        <source>Pressure Support Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Min Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Minimum Therapy Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Max Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Maximum Therapy Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Ramp Delay Period</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Ramp Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Starting Ramp Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3150"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3152"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Ramp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>Vibratory Snore (VS2) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Mask On Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="280"/>
        <source>Time started according to str.edf</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>Summary Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>An apnea where the airway is open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>An apnea caused by airway obstruction</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>Hypopnea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>A partially obstructed airway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>Unclassified Apnea</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>UA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Vibratory Snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>A vibratory snore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7948"/>
        <source>Pressure Pulse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="7949"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="168"/>
        <source>A large mask leak affecting machine performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>Non Responding Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Expiratory Puff</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="174"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="177"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <source>User Flag #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <source>User Flag #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>User Flag #3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="190"/>
        <source>Heart rate in beats per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>SpO2 %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="195"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>Plethysomogram</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="199"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>Pulse Change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="205"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SpO2 Drop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="208"/>
        <source>SD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="212"/>
        <source>Breathing flow rate waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <source>L/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="215"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="218"/>
        <source>Mask Pressure (High resolution)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="221"/>
        <source>Amount of air displaced per breath</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="224"/>
        <source>Graph displaying snore volume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Minute Ventilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="227"/>
        <source>Amount of air displaced per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Respiratory Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="230"/>
        <source>Rate of breaths per minute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Patient Triggered Breaths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="233"/>
        <source>Pat. Trig. Breaths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="236"/>
        <source>Rate of detected mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>I:E Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="240"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="133"/>
        <source>Pressure Min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Pressure Max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Pressure Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>IPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>EPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Cheyne Stokes Respiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>CSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Periodic Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Clear Airway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Obstructive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="153"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>Leak Flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="164"/>
        <source>LF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="180"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="183"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="186"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perfusion Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="202"/>
        <source>Perf. Index %</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Expiratory Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="243"/>
        <source>Time taken to breathe out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Inspiratory Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="246"/>
        <source>Time taken to breathe in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>Respiratory Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="249"/>
        <source>A ResMed data source showing Respiratory Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Graph showing severity of flow limitations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="252"/>
        <source>Flow Limit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="255"/>
        <source>Target Minute Ventilation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Maximum Leak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>The maximum rate of mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="258"/>
        <source>Max Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Apnea Hypopnea Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="261"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Total Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="264"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leak Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median rate of detected mask leakage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="267"/>
        <source>Median Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Respiratory Disturbance Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="270"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="274"/>
        <source>Sleep position in degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="277"/>
        <source>Upright angle in degrees</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="283"/>
        <source>CPAP Session contains summary data only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="287"/>
        <source>PAP Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="287"/>
        <source>PAP Device Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>APAP (Variable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="295"/>
        <source>ASV (Fixed EPAP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="296"/>
        <source>ASV (Variable EPAP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="304"/>
        <source>Physical Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Notes</source>
        <translation type="unfinished">Σημειώσεις</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="305"/>
        <source>Bookmark Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>Body Mass Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>Bookmark Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Bookmark End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>Last Updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Journal Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>Journal</source>
        <translation type="unfinished">Ημερολόγιο διάφορων πράξεων</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Brain Wave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>BrainWave</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Awakenings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="322"/>
        <source>Number of Awakenings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Morning Feel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>How you felt in the morning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time Awake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Time spent awake</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Time In REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Time spent in REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Time in REM Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Time In Light Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Time spent in light sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Time in Light Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time In Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time spent in deep sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Time in Deep Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time to Sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Time taken to get to sleep</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Zeo ZQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>Zeo sleep quality measurement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="329"/>
        <source>ZEO ZQ</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Debugging channel #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Top secret internal stuff you&apos;re not supposed to see ;)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Test #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Debugging channel #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Test #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="734"/>
        <source>Zero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="737"/>
        <source>Upper Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="740"/>
        <source>Lower Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="448"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="185"/>
        <source>Choose the SleepyHead data folder to migrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="186"/>
        <source>or CANCEL to skip migration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="200"/>
        <source>The folder you chose does not contain valid SleepyHead data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="201"/>
        <source>You cannot use this folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source>Migrating </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="216"/>
        <source> files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="217"/>
        <source>to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="435"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="436"/>
        <source>If you have been using SleepyHead, OSCAR can copy your old data to this folder later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="437"/>
        <source>We suggest you use this folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="438"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="444"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="449"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="460"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="482"/>
        <source>Data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="490"/>
        <source>Migrate SleepyHead Data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="491"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="492"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="550"/>
        <source>The version of OSCAR you just ran is OLDER than the one used to create this data (%1).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="552"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="688"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="447"/>
        <source>Exiting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="461"/>
        <source>Are you sure you want to use this folder?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="261"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="261"/>
        <source>OSCAR Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="432"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="433"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="446"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2141"/>
        <source>Sorry, your %1 %2 machine is not currently supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1180"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1233"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="39"/>
        <source>There are no graphs visible to print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="54"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="98"/>
        <source>Printing %1 Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="132"/>
        <source>%1 Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="190"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="240"/>
        <source>RDI	%1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="242"/>
        <source>AHI	%1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="275"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="281"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="285"/>
        <source>UAI=%1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="287"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="356"/>
        <source>Reporting from %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="428"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="434"/>
        <source>Current Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="444"/>
        <source>Entire Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="560"/>
        <source>%1 OSCAR v%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="568"/>
        <source>Page %1 of %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="997"/>
        <source>Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1001"/>
        <source>Low Usage Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1118"/>
        <source>(Sess: %1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1126"/>
        <source>Bedtime: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Waketime: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1168"/>
        <source>90%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1240"/>
        <source>(Summary Only)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="431"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="174"/>
        <source>Peak</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="178"/>
        <source>%1% %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="292"/>
        <source>Fixed Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="293"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1466"/>
        <source>%1%2</source>
        <translation type="unfinished">%1%2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1489"/>
        <source>Fixed %1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1491"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1493"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1508"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1495"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1497"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1501"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1499"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="165"/>
        <location filename="../oscar/SleepLib/day.cpp" line="167"/>
        <location filename="../oscar/SleepLib/day.cpp" line="169"/>
        <location filename="../oscar/SleepLib/day.cpp" line="174"/>
        <source>%1 %2</source>
        <translation type="unfinished">%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="320"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="321"/>
        <source>(last night)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="322"/>
        <source>(yesterday)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="323"/>
        <source>(%2 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="328"/>
        <source>No oximetry data has been imported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>Contec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="39"/>
        <source>CMS50</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>Fisher &amp; Paykel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="77"/>
        <source>ICON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>DeVilbiss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="77"/>
        <source>Intellipap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="85"/>
        <source>SmartFlex Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>ChoiceMMed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="39"/>
        <source>MD300</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="66"/>
        <source>M-Series</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="404"/>
        <source>Philips Respironics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="404"/>
        <source>System One</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="369"/>
        <source>ResMed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="369"/>
        <source>S9</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="378"/>
        <source>EPR: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="37"/>
        <source>Somnopose Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Zeo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="38"/>
        <source>Personal Sleep Coach</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="196"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="373"/>
        <source> (%2 min, %3 sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="375"/>
        <source> (%3 sec)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="387"/>
        <source>Pop out Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1420"/>
        <source>I&apos;m very sorry your machine doesn&apos;t record useful data to graph in Daily View :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1423"/>
        <source>There is no data to graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1608"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2166"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2209"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2280"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2297"/>
        <source>%1</source>
        <translation type="unfinished">%1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2261"/>
        <source>Hide All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2262"/>
        <source>Show All Events</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2603"/>
        <source>Unpin %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2605"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2680"/>
        <source>Popout %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2682"/>
        <source>Pin %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1040"/>
        <source>Plots Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1114"/>
        <source>Duration %1:%2:%3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1115"/>
        <source>AHI %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="108"/>
        <source>%1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="151"/>
        <source>Relief: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="157"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="260"/>
        <source>Machine Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="25"/>
        <source>Journal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="43"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="45"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="46"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="53"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="54"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="56"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F3.7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="41"/>
        <source>CMS50F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1543"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1545"/>
        <source>SmartFlex Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1544"/>
        <source>Intellipap pressure relief mode.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1550"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3041"/>
        <source>Ramp Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1551"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3042"/>
        <source>Full Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1554"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1556"/>
        <source>SmartFlex Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="1555"/>
        <source>Intellipap pressure relief level.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1657"/>
        <source>Parsing Identification File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1759"/>
        <source>Locating STR.edf File(s)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1934"/>
        <source>Cataloguing EDF Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1945"/>
        <source>Queueing Import Tasks...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="2005"/>
        <source>Finishing Up...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3016"/>
        <source>CPAP Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3022"/>
        <source>VPAP-T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3023"/>
        <source>VPAP-S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3024"/>
        <source>VPAP-S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3025"/>
        <source>??</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3026"/>
        <source>VPAPauto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3028"/>
        <source>ASVAuto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3029"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3030"/>
        <source>???</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3031"/>
        <source>Auto for Her</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3034"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3036"/>
        <source>EPR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3035"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3043"/>
        <source>Patient???</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3046"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3048"/>
        <source>EPR Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3047"/>
        <source>Exhale Pressure Relief Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3051"/>
        <source>0cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3052"/>
        <source>1cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3053"/>
        <source>2cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3054"/>
        <source>3cmH2O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3061"/>
        <source>SmartStart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3062"/>
        <source>Machine auto starts by breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3063"/>
        <source>Smart Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3070"/>
        <source>Humid. Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3071"/>
        <source>Humidifier Enabled Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3079"/>
        <source>Humid. Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3080"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3081"/>
        <source>Humidity Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3095"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3097"/>
        <source>Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3096"/>
        <source>ClimateLine Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3102"/>
        <source>Temp. Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3103"/>
        <source>ClimateLine Temperature Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3104"/>
        <source>Temperature Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3113"/>
        <source>AB Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3114"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3115"/>
        <source>Antibacterial Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3122"/>
        <source>Pt. Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3123"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3124"/>
        <source>Patient Access</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3131"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3132"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3133"/>
        <source>Climate Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3137"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3136"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3140"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3142"/>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3141"/>
        <source>ResMed Mask Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3145"/>
        <source>Pillows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3146"/>
        <source>Full Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3147"/>
        <source>Nasal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="3151"/>
        <source>Ramp Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>Weinmann</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="116"/>
        <source>SOMNOsoft2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="350"/>
        <source>Snapshot %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50D+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="266"/>
        <source>CMS50E/F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="228"/>
        <source>%1
Line %2, column %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/updateparser.cpp" line="241"/>
        <source>Could not parse Updates.xml file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="677"/>
        <source>Loading %1 data for %2...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="687"/>
        <source>Scanning Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="721"/>
        <source>Migrating Summary File Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="884"/>
        <source>Loading Summaries.xml.gz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1015"/>
        <source>Loading Summary Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="14"/>
        <source>Please Wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="312"/>
        <source>Peak %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="165"/>
        <source>Updating Statistics cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="691"/>
        <source>Usage Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="512"/>
        <source>Loading summaries</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <location filename="../oscar/reports.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/reports.ui" line="27"/>
        <source>about:blank</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="246"/>
        <source>%1h %2m</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="289"/>
        <source>No Sessions Present</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="532"/>
        <source>CPAP Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="535"/>
        <location filename="../oscar/statistics.cpp" line="1341"/>
        <source>CPAP Usage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="536"/>
        <source>Average Hours per Night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <source>Therapy Efficacy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="549"/>
        <source>Leak Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="554"/>
        <source>Pressure Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="568"/>
        <source>Oximeter Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="572"/>
        <source>Blood Oxygen Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="577"/>
        <source>Pulse Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="588"/>
        <source>%1 Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="589"/>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Average %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="592"/>
        <source>Min %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="593"/>
        <source>Max %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="594"/>
        <source>%1 Index</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>% of time in %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="596"/>
        <source>% of time above %1 threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="597"/>
        <source>% of time below %1 threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="616"/>
        <source>Name: %1, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="618"/>
        <source>DOB: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="621"/>
        <source>Phone: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="624"/>
        <source>Email: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="627"/>
        <source>Address:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1342"/>
        <source>Days Used: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1343"/>
        <source>Low Use Days: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1344"/>
        <source>Compliance: %1%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1368"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1375"/>
        <source>Best AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1379"/>
        <location filename="../oscar/statistics.cpp" line="1391"/>
        <source>Date: %1 AHI: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1385"/>
        <source>Worst AHI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1422"/>
        <source>Best Flow Limitation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1426"/>
        <location filename="../oscar/statistics.cpp" line="1439"/>
        <source>Date: %1 FL: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1432"/>
        <source>Worst Flow Limtation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1444"/>
        <source>No Flow Limitation on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1465"/>
        <source>Worst Large Leaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1473"/>
        <source>Date: %1 Leak: %2%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1479"/>
        <source>No Large Leaks on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1502"/>
        <source>Worst CSR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1510"/>
        <source>Date: %1 CSR: %2%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1515"/>
        <source>No CSR on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1532"/>
        <source>Worst PB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1540"/>
        <source>Date: %1 PB: %2%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1545"/>
        <source>No PB on record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1553"/>
        <source>Want more information?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1554"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1555"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1575"/>
        <source>Best RX Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1578"/>
        <location filename="../oscar/statistics.cpp" line="1590"/>
        <source>Date: %1 - %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1581"/>
        <location filename="../oscar/statistics.cpp" line="1593"/>
        <source>AHI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1582"/>
        <location filename="../oscar/statistics.cpp" line="1594"/>
        <source>Total Hours: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1587"/>
        <source>Worst RX Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1133"/>
        <source>Most Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="537"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="710"/>
        <source>This report was prepared on %1 by OSCAR v%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="713"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="946"/>
        <source>Changes to Machine Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1032"/>
        <source>No data found?!?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1034"/>
        <source>Oscar has no data to report :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1134"/>
        <source>Last Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1135"/>
        <source>Last 30 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1136"/>
        <source>Last 6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1137"/>
        <source>Last Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1141"/>
        <source>Last Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1182"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1196"/>
        <source>No %1 data available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1199"/>
        <source>%1 day of %2 Data on %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1205"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="954"/>
        <source>Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="958"/>
        <source>Pressure Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="960"/>
        <source>Pressure Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="887"/>
        <source>Machine Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="893"/>
        <source>First Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="894"/>
        <source>Last Use</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdaterWindow</name>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="14"/>
        <source>OSCAR Updater</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="60"/>
        <source>A new version of $APP is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="106"/>
        <source>Version Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="117"/>
        <source>Release Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="142"/>
        <source>Build Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="195"/>
        <source>Maybe &amp;Later</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="215"/>
        <source>&amp;Upgrade Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="250"/>
        <source>Please wait while updates are downloaded and installed...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="264"/>
        <source>Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="304"/>
        <source>Component</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="309"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="314"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="319"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="328"/>
        <source>Log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="374"/>
        <source>Downloading &amp; Installing Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.ui" line="394"/>
        <source>&amp;Finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="93"/>
        <source>Updates are not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="127"/>
        <source>Checking for OSCAR Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="146"/>
        <source>Requesting </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <source>OSCAR Updates are currently unvailable for this platform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="158"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <location filename="../oscar/UpdaterWindow.cpp" line="510"/>
        <source>OSCAR Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="459"/>
        <source>Version %1 of OSCAR is available, opening link to download site.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="504"/>
        <source>No updates were found for your platform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="511"/>
        <source>New OSCAR Updates are avilable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="171"/>
        <source>%1 bytes received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="462"/>
        <source>You are already running the latest version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/UpdaterWindow.cpp" line="512"/>
        <source>Would you like to download and install them now?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished">Φόρμα</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="128"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="143"/>
        <source>What would you like to do?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="186"/>
        <source>CPAP Importer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="224"/>
        <source>Oximetry Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="262"/>
        <source>Daily View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="300"/>
        <source>Overview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="338"/>
        <source>Statistics</source>
        <translation type="unfinished">Στατιστικά</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="581"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap machine.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed machine is detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>yesterday</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>%2 days ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>was %1 (on %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="192"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="195"/>
        <source>Your machine was on for %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="208"/>
        <source>under</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="209"/>
        <source>over</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>reasonably close to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>equal to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="225"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="235"/>
        <source>Your CPAP machine used a constant %1 %2 of air</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="238"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="242"/>
        <source>Your machine used a constant %1-%2 %3 of air.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="251"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="252"/>
        <location filename="../oscar/welcome.cpp" line="258"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="257"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="246"/>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="278"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="284"/>
        <source>No CPAP data has been imported yet.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="792"/>
        <source>%1 days</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="392"/>
        <source>100% zoom level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="394"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="396"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="398"/>
        <source>Reset Graph Layout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="399"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="402"/>
        <source>Y-Axis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="403"/>
        <source>Plots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="408"/>
        <source>CPAP Overlays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="411"/>
        <source>Oximeter Overlays</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="414"/>
        <source>Dotted Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1801"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1854"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2099"/>
        <source>Remove Clone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2103"/>
        <source>Clone %1 Graph</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
